// Karma configuration
// Generated on Wed Jun 24 2015 13:08:30 GMT-0500 (CDT)

var envify = require('envify/custom');

module.exports = function(config)
{

	config.set({

		basePath: ''

		, frameworks:
		[

			'browserify'

			, 'mocha'

		]

		, client:
		{

			mocha:
			{

				reporter: 'dot'

			}

		}

		// list of files / patterns to load in the browser
		, files:
		[

			'public/client/build/app.js'

			, 'glhf/tests/**/*.js'

		]

		, exclude:
		[

		]

		, preprocessors:
		{
			'glhf/tests/**/*.js': [ 'browserify' ]
		}

		, browserify:
		{

			debug: true

			, alias:
			{

				'glhf': './glhf/lib/glhf.js'

				, 'glhfStorage': './glhf/lib/glhfStorage.js'

				, 'glhfCache': './glhf/lib/glhfCache.js'

				, 'glhfSocket': './glhf/lib/glhfSocket.js'

				, 'glhfRequestTransports': './glhf/lib/glhfRequestTransports.js'

				, 'glhfRequest': './glhf/lib/glhfRequest.js'

				, 'glhfDataInterface': './glhf/lib/glhfDataInterface.js'

				, 'glhfCollection': './glhf/lib/glhfCollection.js'

				, 'glhfCollectionModel': './glhf/lib/glhfCollectionModel.js'

			}

			, paths:
			[

				'./node_modules/'

				, './glhf/'

				, './glhf/lib/'

				, './app/'

			]

			, transform:
			[

				"browserify-shim"

				, envify( {

					NODE_ENV: process.env.NODE_ENV

				} )

			]

		}

		// test results reporter to use
		// possible values: 'dots', 'progress'
		// available reporters: https://npmjs.org/browse/keyword/karma-reporter
		, reporters: [ 'progress' ]

		// web server port
		, port: 9876

		// enable / disable colors in the output (reporters and logs)
		, colors: true

		// level of logging
		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		, logLevel: config.LOG_DEBUG

		// enable / disable watching file and executing tests whenever any file changes
		, autoWatch: true

		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
		, browsers: [ 'PhantomJS' ]

		, plugins:
		[

			'karma-phantomjs-launcher'

			// , 'karma-chrome-launcher'

			, 'karma-browserify'

			, 'karma-mocha'

		]

		// Continuous Integration mode
		// if true, Karma captures browsers, runs the tests and exits
		, singleRun: true

	});

};

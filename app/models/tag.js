module.exports=
{

	identity: 'tags'

	, connection: 'default'

	, tableName: "tags"

	, attributes:
	{

		iden: 'string'

		, name:
		{

			type: 'string'

			, required: true

			 , unique: true
		}

		, color: 'string'

		, slug: 'string'

		, Tproyectos:
		{

			collection: 'proyectos'

			, via: 'tags'

		}

		, Tcollabos:
		{

			collection: 'collabos'

			, via: 'tags'

		}

		, Tevents:
		{

			collection: 'events'

			, via: 'tags'

		}

	}

};
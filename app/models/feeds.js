
module.exports =
{

	identity: 'feeds'

	, connection: 'default'

	, tableName: "feeds"

	, attributes:
	{

		profile: { model: 'profiles' }

		, from: { model: 'profiles' }

		, post: { model: 'posts' }

		, own: { type: 'boolean', defaultsTo: false, index: true }

		, featured: { type: 'boolean', defaultsTo: false, index: true }

		, hidden: { type: 'boolean', defaultsTo: false, index: true }

		, seen: { type: 'boolean', defaultsTo: false, index: true }

		, deleted: { type: 'boolean', defaultsTo: false, index: true }

		, open: { type: 'boolean', defaultsTo: true, index: true }

		, direct: { type: 'boolean', defaultsTo: false, index: true }

	}

};

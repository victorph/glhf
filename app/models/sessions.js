/*
	Model sessions
*/

'use strict';

module.exports =
{

	connection: 'default'

	, identity: 'sessions'

	, attributes:
	{

		session: 'json'

		, expires: 'datetime'

	}

};

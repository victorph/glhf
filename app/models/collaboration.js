module.exports=
{

	identity: 'collabos'

	, connection: 'default'

	, tableName: "collabos"

	, attributes:
	{

		name:
		{

			type: 'string'

			, required: true

		}

		, cont: 'string'

		, intr: 'string'

		, adre: 'string'

		, line: 'string'

		, feat: 'string'

		, date: 'string'

		, fullAdre: 'string'

		, media: "json"

		, kind: 'array'

		, portada: { model: "portadas" }

		, categ: "array"

		// , tags: "array"

		, activities:
		{

			collection: 'events'

			, via: 'owner'

			, dominant: true

		}

		, tags:
		{

			collection: 'tags'

			, via: 'Tcollabos'

			, dominant: true

		}

	}

};

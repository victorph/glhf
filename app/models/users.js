
"use strict();";

var User =
{

	identity: 'users'

	, connection: 'default'

	, tableName: "users"

	, conf:
	{

		paginate: { limit: 3 }

		, baseUrl: '/api/user'

		, syncRate: 5

		, modelSyncRate: 10

		, autosave: false

		, allowed:
		{

			get:
			[

				'id'

				, 'email'

				, 'name'

				, 'slug'

				, 'profile'

				, 'studentof'

				, 'teacherof'

			]

			, post:
			[

				'email'

				, 'name'

			]

			, put:
			[

				'studentof'

				, 'teacherof'

				, 'name'

			]

		}

	}

	, attributes:
	{

		id:
		{

			type: 'integer'

			, autoIncrement: true

			, primaryKey: true

			, unique: true

		}

		, email: "email"

		, password: 'string'

		, slug: "string"

		, name: "string"

		, profile:
		{

			collection: 'profiles'

			, via: 'user'

		}

		, studentof:
		{

			collection: 'events'

			, via: 'students'

		}

		, teacherof:
		{

			collection: 'events'

			, via: 'teacher'

		}

		, toJSON: function ()
		{

			var obj = this.toObject();

			delete obj.password;

			return obj;

		}

	}

	, beforeCreate: function ( values, next )
	{

		var bcrypt = require('bcrypt');

		bcrypt.genSalt( 10, function ( err, salt ) {

			if (err) return next(err);

			bcrypt.hash( values.password, salt, function ( err, hash ) {

				if (err) return next(err);

				values.password = hash;

				next();

			});

		});

	}

};

module.exports = User;


module.exports =
{

	identity: 'dislikes'

	, connection: 'default'

	, tableName: "dislikes"

	, attributes:
	{

		profile:
		{

			model: 'profiles'

		}

		, model: 'string'

		, iden: 'string'

	}

};

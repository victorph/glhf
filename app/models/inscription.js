
module.exports =
{

	identity: 'inscripciones'

	, connection: 'default'

	, tableName: "inscripciones"

	, attributes:
	{

		title: 'string'

		, desc: 'string'

		, active: 'string'

		, openAt: 'string'

		, closeAt: 'string'

		, needInvit: 'string'

		, needResponse: 'string'

		, fields: 'json'

		, countReceived: 'string'

		, countWaiting: 'string'

		, countAccepted: 'string'

		, countRejected: 'string'

		, mail_accepted: { model: 'MailTemplate' }

		, mail_rejected: { model: 'MailTemplate' }

		, mail_recieved: { model: 'MailTemplate' }

		, labelSend: 'string'

		, msgOk: 'string'

		, evento:
		{

			collection: 'events'

			, via: 'inscripciones'

			, dominant: true

		}

	}

};

module.exports=
{

	identity: 'proyectos'

	, connection: 'default'

	, tableName: "proyectos"

	, attributes:
	{

		name:
		{

			type: 'string'

			, required: true

		}

		, date: 'string'

		, adre: 'string'

		, fullAdre: 'string'

		, line: 'string'

		, feat: 'string'

		, cont: 'string'

		, media: "json"

		, kind: 'array'

		, portada: { model: "portadas" }

		, tags:
		{

			collection: 'tags'

			, via: 'Tproyectos'

			, dominant: true

		}

		, activities:
		{

			collection: 'events'

			, via: 'owner'

			, dominant: true

		}

	}

};

module.exports =
{

	identity: 'privacy'

	, connection: 'default'

	, tableName: "privacy"

	, attributes:
	{

		profile: 'string'

		, what: 'string'

		, id: 'string'

		, settings: 'string'

	}

};

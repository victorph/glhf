
module.exports=
{

	identity: 'MailTemplate'

	, connection: 'default'

	, tableName: "MailTemplate"

	, attributes:
	{

		title: 'string'

		, text: 'string'

		, html: 'string'

		, composite:
		{

				type: 'string'

				, unique: true

		}

	}

};

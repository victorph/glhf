var chance = new require('chance')( Math.random );

module.exports =
{

	identity: 'profiles'

	, connection: 'default'

	, tableName: "profiles"

	, attributes:
	{

		id:
		{

			type: 'integer'

			, autoIncrement: true

			, primaryKey: true

			, unique: true

		}

		, name: 'string'

		, user:
		{

			collection: 'users'

			, via: 'profile'

		}

		, nickname: 'string'

		, dob: 'datetime'

		, avatar: 'string'

		, bg: 'string'

		, about: 'string'

		, web: 'string'

		, facebook: 'string'

		, twitter: 'string'

		, google: 'string'

		, instagram: 'string'

		, comments:
		{

			collection: 'comments'

			, via: 'profile'

		}

		, feed:
		{

			collection: 'feeds'

			, via: 'profile'

		}

		, projects: 'array'

		, followers:
		{

			collection: 'profiles'

			, via: 'following'

		}

		, following:
		{

			collection: 'profiles'

			, via: 'followers'

		}

		, events:
		{

			collection: 'events'

			, via: 'owner'

		}

		, bookmarks:
		{

			collection: 'bookmarks'

			, via: 'owner'

		}

		, interests: 'array'

		, agenda:
		{

			collection: 'agenda'

			, dominant: true

		}

		, likes:
		{

			collection: 'likes'

			, via: 'profile'

		}

		, dislikes:
		{

			collection: 'dislikes'

			, via: 'profile'

		}

		, rdm: { type: 'number' , unique:true, defaultsTo: chance.floating( { min: 0, max: 1, limit: 8 } ) }

	}

};


module.exports =
{

	identity: 'bookmarks'

	, connection: 'default'

	, tablename: 'bookmarks'

	, attributes:
	{

		platform: 'string'

		, data: 'string'

		, item: 'string'

		, owner: { model: 'profiles' }

	}

};

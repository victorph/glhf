
module.exports =
{

	identity: 'materials'

	, connection: 'default'

	, tableName: "materials"

	, attributes:
	{

		name:
		{

			type: 'string'

			, required: true

		}

		, owner:
		{

			collection: 'calendars'

			, via: 'materials'

		}

	}

};

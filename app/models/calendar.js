
module.exports =
{

	identity: 'calendars'

	, connection: 'default'

	, tableName: "calendars"

	, attributes:
	{

		start:
		{

			type: 'string'
		}

		, end:
		{

			type: 'string'
		}

		, owner:
		{

			collection: 'events'

			, via: 'calendar'

		}

		, materials:
		{

			type: 'string'

		}

		, kind:
		{

			type: 'string'

		}

		, location:
		{

			type: 'string'

		}

		, attendance:
		{

			type: 'json'

		}

		, note:
		{

			type: 'string'

		}

	}

};

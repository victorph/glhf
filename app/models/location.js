
module.exports =
{

	identity: 'venues'

	, connection: 'default'

	, tableName: "Venues"

	, attributes:
	{

		name:
		{

			type: 'string'

			, required: true

		}

		, using:
		{

			collection: 'events'

			, via: 'location'

		}

	}

};

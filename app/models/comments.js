module.exports=
{

	identity: 'comments'

	, connection: 'default'

	, tableName: "comments"

	, attributes:
	{

		profile: { model: 'profiles' }

		, content: 'string'

		, model:'string'

		, iden:'string'

		, picture: 'string'

		, likes: 'array'

		, dislikes: 'array'

		, deleted: 'boolean'

		, total:
		{

			likes: { type: 'number', defaultsTo: 0, index: true },

			dislikes: { type: 'number', defaultsTo: 0, index: true },

			flags: { type: 'number', defaultsTo: 0, index: true }

		}

	}

};

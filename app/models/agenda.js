
module.exports =
{

	identity: 'agenda'

	, connection: 'default'

	, tablename: 'agenda'

	, attributes:
	{

		title: 'string'

		, intro: 'string'

		, content: 'string'

		, profile: { model: 'profiles' }

		, project: { model: 'proyectos' }

		, date_start: { type: 'datetime', required: true }

		, date_end: { type: 'datetime', required: true }

	}

};

var role=
{

	identity: 'role'

	, connection: 'default'

	, tableName: "role"

	, conf:
	{

		paginate: { limit: 3 }

		, baseUrl: '/api/role'

		, syncRate: 5

		, modelSyncRate: 10

		, autosave: false

		, allowed:
		{

			get:
			[



			]

			, post:
			[

				'name'

			]

			, put:
			[

				'name'

			]

		}

	}

}

role.attributes=
{

	name:
	{

		type: 'string'

		, defaultsTo: 'guest'

	}

	, rights:
	{

		type: 'json'

		, defaultsTo: JSON.stringify( {} )

	}

	, email_validate:
	{

		type: 'boolean'

		, defaultsTo: false

	}

}

role.beforeCreate = function roleBeforeCreate ( self, next )
{

	console.log( self, next );

	next();

};

module.exports = role;

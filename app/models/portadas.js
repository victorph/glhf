module.exports =
{

	identity: 'portadas'

	, connection: 'default'

	, tableName: "portadas"

	, attributes:
	{

		name:"string"

		, pic:"json"

	}

};
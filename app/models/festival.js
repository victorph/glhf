
module.exports=
{

	identity: 'festival'

	, connection: 'default'

	, tableName: "festivals"

	, attributes:
	{

		name:
		{

			type: 'string'

			, required: true

		}

		, cont: 'string'

		, intr: 'string'

		, categ: "array"

		, portada: { model: "portadas" }

		, activities:
		{

			collection: 'events'

			, via: 'owner'

			, dominant: true

		}

	}

};

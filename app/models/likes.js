
module.exports =
{

	identity: 'likes'

	, connection: 'default'

	, tableName: "likes"

	, attributes:
	{

		profile:
		{

			model: 'profiles'

		}

		, model: 'string'

		, iden: 'string'

	}

};


module.exports =
{

	identity: 'posts'

	, connection: 'default'

	, tableName: "posts"

	, attributes:
	{

		profile:
		{

			model: 'profiles'

			, index: true

			, required: true

		}

		, content: 'string'

		, cover: 'string'

		, gallery: 'string'

		, likes: 'array'

		, dislikes: 'array'

		, tags: 'array'

		, comments: 'array'

		, privacy: 'string'

		, feeded: { type: 'boolean', defaultsTo: false, index: true }

		, total:
		{

			featured: { type: 'number', defaultsTo: 0, index: true }

			, repost: { type: 'number', defaultsTo: 0, index: true }

			, likes: { type: 'number', defaultsTo: 0, index: true }

			, dislikes: { type: 'number', defaultsTo: 0, index: true }

			, comments: { type: 'number', defaultsTo: 0, index: true }

			, flags: { type: 'number', defaultsTo: 0, index: true }

			, seen:  { type: 'number', defaultsTo: 0, index: true }

		}

	}

};
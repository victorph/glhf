
module.exports =
{

	identity: 'results'

	, connection: 'default'

	, tableName: "results"

	, attributes:
	{

		name: 'string'

		, url: 'string'

		, event:
		{

			collection: 'events'

			, via: 'results'

		}

	}

};

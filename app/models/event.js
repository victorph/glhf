
module.exports  =
{

	identity: 'events'

	, connection: 'default'

	, tableName: "events"

	, attributes:
	{

		id:
		{

			type: 'integer'

			, autoIncrement: true

			, primaryKey: true

			, unique: true

		}

		, name: 'string'

		, fullAdre: 'string'

		, adre: 'string'

		, cont: 'string'

		, intr: 'string'

		, titu: 'string'

		, slug: 'string'

		, kind: 'array'

		, link: 'string'

		, iden: 'string'

		, feat: 'string'

		, line: 'string'

		, inscIden: 'string'

		, tagsA: 'array'

		, categ: "array"

		, starts: "string"

		, ends: "string"

		, studentLimit: 'string'

		, studentTotal: 'string'

		, data:'json'

		, status: 'string'

		, studentList: 'json'

		, media: 'json'

		, memories: 'json'

		, inscr: 'json'

		, extra: 'string'

		, extra_titu: 'string'

		, tags:
		{

			collection: 'tags'

			, via: 'Tevents'

			, dominant: true

		}

		, students:
		{

			collection: 'users'

			, via: 'studentof'

			, dominant: true

		}

		, teacher:
		{

			collection: 'users'

			, via: 'teacherof'

			, dominant: true

		}

		, calendar:
		{

			collection: 'calendars'

			, via: 'owner'

			, dominant: true

		}

		, location:
		{

			collection: 'venues'

			, via: 'using'

			, dominant: true

		}

		, owner:
		{

			collection: 'collabos'

			, via: 'activities'

		}

		, proyecto:
		{

			collection: 'proyectos'

			, via: 'activities'

		}

		, inscripciones:
		{

			collection: 'inscripciones'

			, via: 'evento'

		}

		, results:
		{

			collection: 'results'

			, via: 'event'

		}

	}

};

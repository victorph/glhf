
var glhf = require('glhf')
	, m = require('mithril');

var search =
{

	identity: 'search'

	, actions:
	{

		onchange: function add ()
		{

			console.log( 'set current to', this.value );

			search.current = this.value;

			console.log( search.current );

		}

		, onsearch: function sendRequest ()
		{

			search.current = this.value;

			console.log( 'current search', search.current );

			this.value = "";

			search.current = "";

		}

	}

	, controller: function ()
	{

		var self = search;

		return self;

	}

	, view: function ( ctrl )
	{

		m.render( document.getElementById('navSearch'), m( 'input#search.search[type="search"][name="search"]', ctrl.actions ) );

	}

};

module.exports = search;

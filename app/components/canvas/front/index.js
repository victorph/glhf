/*

	Component: canvas

*/

var glhf = require('glhf')
	, glhfComponent = require('glhfComponent')
	, canvas = require('./../models');

// associated with mithril...

canvas.controller = function ( opts )
{

	var self = canvas;

	return self;

};

// associated with mithril...

canvas.view = function ( ctrl )
{

	var view;

	document.getElementById( canvas.target ).innerHTML = glhf.views.component.canvas( ctrl );

	return view;

};

// called on page change.

canvas.off = function ( ctrl )
{

	var view;

	document.getElementById( canvas.target ).innerHTML = glhf.views.component.canvas( ctrl );

	return view;

};

// called on page init.

canvas.on = function ( ctrl )
{

};

glhf.onready(function(){

	canvas = new glhfComponent( canvas );

});

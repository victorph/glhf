/*

	Component: canvas
	Model

*/

var canvas =
{

	identity: 'canvas'

	, target: 'canvas'

	, components: {}

	, attributes: {}

};

module.exports = canvas;


var m = require('mithril')
	, glhf = require('glhf')
	, glhfComponent = require('glhfComponent');

var navbar =
{

	identity: 'navbar'

	, target: 'UINavBar'

	, components:
	{

		search: 'search'

		, alertCenter: 'alertCenter'

		, profileCenter: 'profileCenter'

	}

};

function fadeOut ()
{

	$('.navbar').css('opacity',0.2);

}

function fadeIn ()
{

	$('.navbar').css('opacity',1);

}

navbar.off = function ()
{
	fadeOut();
};

navbar.controller = function ( opts )
{

	var self = navbar;

	return self;

};

navbar.view = function ( ctrl )
{

	var view;

	document.getElementById( navbar.target ).innerHTML = glhf.views.component.navbar( ctrl );

	return view;

};

Object.defineProperty( navbar, 'fadeIn', { value: fadeIn } );

Object.defineProperty( navbar, 'fadeOut', { value: fadeOut } );

glhf.onready(function(){

	navbar = new glhfComponent( navbar );

});

module.exports = navbar;

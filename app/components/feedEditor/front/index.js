
var _ = require('lodash')
	, m = require('mithril')
	, async = require('async')
	, glhf = require('glhf');

var feedEditor =
{

};

feedEditor.textareaChanged= function ( ev )
{

};

feedEditor.uploadMediaClicked = function ( ev )
{

};

feedEditor.publishClicked = function ( ev )
{

};

feedEditor.feedEditorHovered = function ( ev )
{

};

feedEditor.controller = function ( )
{

	var self =
	{

	};

	return self;

};

feedEditor.view = function ( ctrl )
{

	var view;

	// document.getElementById('secondApp').innerHTML += JST.component.feedEditor();

	view =
	[

		m(".panel.panel-default.profile-feed",
		{
			onmouseover: feedEditor.feedEditorHovered
		}

		,[ m(".feed-post-create",
			[

				m("textarea.feed-post-textarea",
				{

					onchange: feedEditor.textareaChanged

					, 'data-model': 'Post'

					, 'data-mywall': true

				} )

				, m(".feed-post-controls",
				[

					m("a.btn.btn-default.upload-image.pull-left",
					{

						style: { "display": "inline-table" }

						, onclick: feedEditor.uploadMediaClicked

					}

					, [

						m("span.glyphicon.glyphicon-picture")

					]),

					m("div.feed-editor-thumbs", { } )

					, m("a.post-feed-action.btn.btn-default.btn-info.pull-right", {

						onclick: feedEditor.publichClicked

					} , "PUBLICAR" )

				])

			])
		])

	];

	m.render( document.getElementById('secondApp'), view );

};

glhf.onready(function()
{
	// feedEditor.view( feedEditor.controller );
});

module.exports = feedEditor;

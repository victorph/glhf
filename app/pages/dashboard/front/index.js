
var glhf = require('glhf')
	, glhfPage = require('glhfPage')
	, pageObject
	, m = require('mithril')
	, dashboard =
	{

		identity: 'dashboard'

		, baseUrl: '/dashboard'

		, components:
		{

			navbar: 'navbar'

			, search: 'search'

			, alertCenter: 'alertCenter'

			, profileCenter: 'profileCenter'

		}

	};

dashboard.controller = function ()
{

	var data = {};


	return {};

};

dashboard.view = function ( ctrl )
{

	var view=[];

	return view;

};

dashboard.init = function ()
{

	glhf.canvas.innerHTML = glhf.views.page.dashboard();

	dashboard.components.navbar.view(dashboard.components.navbar.controller());

	dashboard.components.navbar.components.search.view(dashboard.components.navbar.components.search.controller());

	dashboard.components.navbar.components.alertCenter.view(dashboard.components.navbar.components.alertCenter.controller());

	dashboard.components.navbar.components.profileCenter.view(dashboard.components.navbar.components.profileCenter.controller());

	dashboard.components.navbar.fadeIn();

};

glhf.onready( function ()
{

	pageObject = glhfPage( dashboard );

});

module.exports = dashboard;


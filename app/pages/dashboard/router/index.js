
'use strict';

var express = require('express')
	, router = express.Router();

module.exports = function ( conf )
{

	router.get('/dashboard', require( './get' ).bind( { models: conf.models } ) );

	return router;

};
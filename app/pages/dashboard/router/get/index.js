
module.exports = function ( req, res, next )
{

	if ( req.xhr === true )
	{

		if ( req.user ) return res.json( { location: '/dashboard' } );

		return res.json(  { location: '/auth/login' }  );

	}

	else
	{

		if ( req.user ) return res.render( 'client' );

		return res.redirect( '/auth/login' );

	}

};

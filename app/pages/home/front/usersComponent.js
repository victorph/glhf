
var _ = require('lodash')
	, async = require('async')
	, m = require('mithril')
	, glhf = require( 'glhf' )
	, glhfComponent = require( 'glhfComponent' )
	, glhfCollection = require( 'glhfCollection' );

usersComponent = new glhfComponent( {

	identity: "usersComponent"

	, target: 'userList'

	, controls: 'userListControls'

	, collections:
	{

		User: 'glhf.models.db.user'

		, Role: 'glhf.models.db.role'

	}

	, dataset: []

	, redraw: m.redraw

	, controller: function ( fetch )
	{

		m.startComputation();

		$('#usersComponent').fadeIn();

		if ( _.isUndefined( fetch ) )
		{

			usersComponent.collections.User.find(
			{

				paginate:
				{

					page: usersComponent.collections.User.paginate().current

					, limit: usersComponent.collections.User.paginate().page.limit

				}

			}, function ( err, docs )
			{

				usersComponent.dataset = docs;

				m.endComputation();

			} );

		}

		else
		{

			usersComponent.dataset = usersComponent.collections.User.dataset;

			m.endComputation();

		}

	}

	, view: function ()
	{

		var user_list;

		m.render( document.getElementById( usersComponent.controls ),
		[

			m( 'a#usersListPrev', {

				href: '#'

				, onclick: function ( ev )
				{

					ev.preventDefault();

					usersComponent.collections.User.paginate().prevPage();

				}

			}, 'Previous' )

			, ' '

			, m( 'a#usersListNext', {

				href: '#'

				, onclick: function ( ev )
				{

					ev.preventDefault();

					usersComponent.collections.User.paginate().nextPage();

				}

			}, 'Next' )

			, ' Page (' + usersComponent.collections.User.paginate().current + ') '

		] );

		user_list = usersComponent.dataset.map( function ( user )
		{

			// m("input", {onchange: m.withAttr("value", controller.user.name), value: controller.user.name()})

			return m( "a.list-group-item",
				{
					'data-status': user.status

					, 'data-user': user.id

					, 'data-slug': user.slug

					, 'data-online': user.online

					, onclick: function( ev )
					{

						ev.preventDefault();

						$('a.list-group-item').toggleClass('active');

						$(this).toggleClass('active');

						$('a.list-group-item').toggleClass('active');

						// $( '#qEd' ).modal('toggle');

					}

				}

				, [
					m( "h4.list-group-item-heading"
					, {

						href: '/profile/' + user.slug

						, 'data-userid': user.id

						, onclick: function( ev )
						{

							ev.preventDefault();

						}

					}, user.email )

					, m( 'div.list-group-item-text'
					, {

					}, [

						m( 'p', {}, user.status )

						, m( 'p', {}, user.id )

						, m( 'p', {}, user.slug )

						, m( 'p', {}, user.online ? 'online' : 'offline' )

					] )

				]

			);

		});

		return m( "div#usersComponent.list-group", user_list );

	}

} );

glhf.onready( function ()
{

	// usersComponent.collections.User.on( 'modelChanged', function ()
	// {

	// 	usersComponent.redraw();

	// } );

	// usersComponent.collections.User.on( 'modelSaved', function ()
	// {

	// 	usersComponent.view(usersComponent.controller());

	// } );

	// usersComponent.collections.User.on( 'modelDeleted', function ()
	// {

	// 	usersComponent.view(usersComponent.controller());

	// } );

} );

module.exports = usersComponent;

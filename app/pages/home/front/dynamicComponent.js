var _ = require('lodash')
	, async = require('async')
	, m = require('mithril');

var dynamicComponent =
{

	target: 'dynamicComponent'

	, models:
	{

		count:
		{

			count: 0

			, target: 'dynamicComponentCount'

			, actions:
			{

				inc: function ()
				{

					var self = dynamicComponent.models[ this.name ];

					self.count++;

					self.refresh();

					return this;

				}

				, dec: function ()
				{

					var self = dynamicComponent.models[ this.name ];

					self.count--;

					self.refresh();

					return this;

				}

				, dob: function ()
				{

					var self = dynamicComponent.models[ this.name ];

					self.count += self.count;

					self.refresh();

					return this;

				}

				, res: function ()
				{

					var self = dynamicComponent.models[ this.name ];

					self.count = 0;

					self.refresh();

					return this;

				}

			}

			, refresh: function ()
			{

				document.getElementById( this.target ).innerHTML = this.count;

			}

		}

	}

	, controller: function(data)
	{

		var self =
		{

			click: function ( ev )
			{

				ev.preventDefault();

				if ( ! _.isUndefined( dynamicComponent.models[ this.name ] ) )
				{

					var model = dynamicComponent.models[ this.name ];

					if ( _.isFunction( model.actions[ this.dataset.action ] ) )
					{

						var action = model.actions[ this.dataset.action ];

						return action.bind( _.assign( this.dataset, {

							name: this.name

						} ) )();

					}

				}

			}

		};

		return self;

	}

	, view: function(ctrl)
	{

		return [

			m( 'a[href=javascript:;][name="count"][data-action="inc"]',
			{

				onclick: ctrl.click

			}, 'Add' )

			, ' '

			, m( 'a[href=javascript:;][name="count"][data-action="dec"]',
			{

				onclick: ctrl.click

			}, 'Rest' )

			, ' '

			, m( 'a[href=javascript:;][name="count"][data-action="dob"]',
			{

				onclick: ctrl.click

			}, 'Double' )

			, ' '

			, m( 'a[href=javascript:;][name="count"][data-action="res"]',
			{

				onclick: ctrl.click

			}, 'Reset' )

		]

	}

};

module.exports = dynamicComponent;

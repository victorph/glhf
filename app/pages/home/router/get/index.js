
module.exports = function ( req, res, next )
{

	if ( req.xhr === true )
	{

		if ( req.user )
		{

			return res.json( { location: '/dashboard', reload: true } );

		}

		return res.json(  { location: '/', reload: true } );

	}

	else
	{

		if ( req.user )
		{

			return res.redirect( '/dashboard' );

		}

		return res.render( 'client' );

	}

};


'use strict';

var express = require('express')
	, router = express.Router()
	, home = require('./../models');

module.exports = function ( conf )
{

	router.get( home.baseUrl, require( './get' ).bind( { models: conf.models } ) );

	return router;

};
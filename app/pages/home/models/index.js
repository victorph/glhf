
module.exports =
{

	identity: 'home'

	, view: 'home'

	, baseUrl: '/'

	, components:
	{
		'navbar': 'navbar'
	}

};

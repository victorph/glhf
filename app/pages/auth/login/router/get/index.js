module.exports = function ( req, res, next )
{

	if ( req.xhr === true )
	{

		if ( req.user ) return res.redirect( { location: '/dashboard' } );

		return res.json( { location: '/auth/login' } );

	}

	else
	{

		if ( req.user ) return res.redirect( '/dashboard' );

		return res.render( 'client' );

	}

};

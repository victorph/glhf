
module.exports = function ( req, res, next )
{

	req.logout();

	if ( req.xhr === true ) return res.json( { location: '/' } );

	return res.redirect('/');

};

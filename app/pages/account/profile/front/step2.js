
module.exports=function()
{

	$("#stepsLocation").geocomplete(
	{

		details: '#stepsLocation'

	}).bind("geocode:result",function(event,result){

		var _data=
		{
			type: 'update',
			sub: 'location',
			cookie: $.cookie('overload.session'),
			body:
			{
				location:
				{
					country: '',
					state: '',
					geo: []
				}
			}
		};

		// search google data

		for (var i = 0 ; i < result.address_components.length ; i++)
		{

			if (_.contains(result.address_components[i].types, 'locality'))
			{

				_data.body.location.city = result.address_components[ i ].long_name;

			}

			else if (_.contains(result.address_components[i].types, 'country'))
			{

				_data.body.location.country = result.address_components[ i ].long_name;

			}

		}

		_data.body.location.geo=[result.geometry.location.D,result.geometry.location.k];

		console.log(_data);

	});

	$('.form-element').off('blur').on('blur',function(){

		var _data={
			type: 'update',
			sub: 'data',
			cookie: '',
			body: {}
		};

		var $el=$(this),self=this;

		if($el.val().length===0)return;

		switch(self.id){

			case 'stepsWeb':
				$('.cont-icon-web').html('<i class="icon-spin-5 animate-spin"></i>');
				_data.body.web=$el.val();
				break;

			case 'stepsFacebook':
				$('.cont-icon-facebook').html('<i class="icon-spin-5 animate-spin"></i>');
				_data.body.facebook=$el.val();
				break;

			case 'stepsTwitter':
				$('.cont-icon-twitter').html('<i class="icon-spin-5 animate-spin"></i>');
				_data.body.twitter=$el.val();
				break;

			case 'stepsGoogle':
				$('.cont-icon-gplus').html('<i class="icon-spin-5 animate-spin"></i>');
				_data.body.google=$el.val();
				break;

			case 'stepsInstagram':
				$('.cont-icon-instagram').html('<i class="icon-spin-5 animate-spin"></i>');
				_data.body.instagram=$el.val();
				break;

			case 'stepsAbout':
				_data.body.about=$el.val();
				break;
		}

		console.log(_data);

/*
		overload.IO.socket.emit('profile',_data,function(err,res){

			if(_.isNull(err)){
				if(process.env.NODE_ENV=='development') console.log('profile step2 save res:',res);
				switch(self.id){

					case 'stepsWeb':

						$('.cont-icon-web').html('<i class="icon-check"></i>').css('color', 'green').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-web').html('<i class="icon-link"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsFacebook':

						$('.cont-icon-facebook').html('<i class="icon-check"></i>').css('color', 'green').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-facebook').html('<i class="icon-facebook"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsTwitter':

						$('.cont-icon-twitter').html('<i class="icon-check"></i>').css('color', 'green').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-twitter').html('<i class="icon-twitter"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsGoogle':

						$('.cont-icon-gplus').html('<i class="icon-check"></i>').css('color', 'green').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-gplus').html('<i class="icon-gplus"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsInstagram':

						$('.cont-icon-instagram').html('<i class="icon-check"></i>').css('color', 'green').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-instagram').html('<i class="icon-instagram"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsAbout':
						break;
				}
			}else{

				switch(self.id){

					case 'stepsWeb':

						$('.cont-icon-web').html('<i class="icon-cancel-1"></i>').css('color', 'red').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-web').html('<i class="icon-link"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsFacebook':

						$('.cont-icon-facebook').html('<i class="icon-cancel-1"></i>').css('color', 'red').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-facebook').html('<i class="icon-facebook"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsTwitter':

						$('.cont-icon-twitter').html('<i class="icon-cancel-1"></i>').css('color', 'red').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-twitter').html('<i class="icon-twitter"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsGoogle':

						$('.cont-icon-gplus').html('<i class="icon-cancel-1"></i>').css('color', 'red').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-gplus').html('<i class="icon-gplus"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsInstagram':

						$('.cont-icon-instagram').html('<i class="icon-cancel-1"></i>').css('color', 'red').delay(1000).animate({
							opacity:0
						}, {
							complete: function() {
								$('.cont-icon-instagram').html('<i class="icon-instagram"></i>').css('color', '#555').animate({
									opacity:1
								});
							}
						});

						break;

					case 'stepsAbout':
						break;
				}


				if(overload.DEBUG===true)console.error('%cprofile step2 save error:','font-size:12px;color:red;',err);
			}

		});
*/
	});

};
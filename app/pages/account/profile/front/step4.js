
module.exports = function()
{

	$('a[data-action="follow-user"]').off('click').on('click', function(e) {

		e.preventDefault();

		$(this).html('<i class="icon-spin5"></i>');
		$(this).off('click');

		var $self = $(this),
			_name = $self.data('name')||$self.data('nickname');

		var _d={
			type:'add',
			profile:$self.data('id')
		};

		overload.IO.emit('friends',_d,function(err,res){

			if(_.isNull(err)){
				if(_.isBoolean(res)){
					if(res===true) {
						alertCenter.add({ content: 'Ahora estas siguiend a '+_name+'!' });
						$self.fadeOut('normal', function() {
							$self.html('SIGUIENDO').css({backgroundColor: 'grey'}).fadeIn();
						});
					}
					else {
						alertCenter.add({ content: 'No se pudo agregar a '+_name+'!' });
						$self.fadeOut('normal', function() {
							$self.html('ERROR').css({backgroundColor: 'red'}).fadeIn();
						});
					}
				}
				else {
					alertCenter.add({ content: 'Ya estas siguiendo a '+_name+'!' });
					$self.fadeOut('normal', function() {
						$self.html('SIGUIENDO').css({backgroundColor: 'grey'}).fadeIn();
					});
				}
			}
			else {
				alertCenter.add({ content: 'No se pudo agregar a '+_name+'!' });
				$self.fadeOut('normal', function() {
					$self.html('ERROR').css({backgroundColor: 'red'}).fadeIn();
				});
			}

		});

	});

};

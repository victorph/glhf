
/* global $:true */

'use strict';

var glhf = require('glhf')
	, glhfPage = require('glhfPage')
	, step2Script = require('./step2.js')
	, step3Script = require('./step3.js')
	, step4Script = require('./step4.js')
	// , m = require('mithril')
	, accountProfile =
	{

		identity: 'accountProfile'

		, baseUrl: '/account/profile'

		, components: {}

		, current: 'step1'

	};

var __advanceMenu = function ( step )
{

	var c=0;

	$('#'+step+'Menu')
	.find('span')
	.each( function ( k, v )
	{

		if(c<3)
		{
			$(v).addClass('active-color');
		}

		c += 1;

	} );

	return true;
};

var ___navComponent = function()
{

	// Skip current step.

	$( '.jump-step' ).off( 'click' ).on( 'click', function ( ev )
	{

		ev.preventDefault();

		var next = this.dataset.next;

		$('.step-container').fadeOut( 800, function()
		{

			setTimeout(function()
			{

				$('#'+next).fadeIn( 800 );

				__advanceMenu(next);

				return true;

			},1200);

			return true;

		} );

		return false;

	});


	// Save current step data and continue to the next.

	$('.continue').off('click').on('click', function ( ev )
	{

		ev.preventDefault();

		var next = this.dataset.next;

		$('.step-container').fadeOut( 800, function(){

			setTimeout(function(){

				__advanceMenu(next);

				$('#'+next).fadeIn( 800, function(){

					switch(next)
					{

						case 'step2':

							step2Script();

							break;

						case 'step3':

							step3Script();

							break;

						case 'step4':

							step4Script();

							break;

					}

					return true;

				});

				return true;

			},1200);

			return true;

		});

		return false;

	});

	return true;

};

accountProfile.controller = function ()
{

	var data = {};

	return data;

};

accountProfile.view = function ( ctrl )
{

	var view = ctrl;

	return view;

};

accountProfile.init = function ()
{

	$('#canvas').remove();

	glhf.canvas.innerHTML = glhf.views.page.accountProfile();

	accountProfile.components.navbar = require( 'components/navbar/front' );

	accountProfile.components.navbar.view(accountProfile.components.navbar.controller());

	accountProfile.components.navbar.fadeIn();

	console.log( 'navbar', accountProfile.components.navbar );

	console.log( 'accountProfile', accountProfile );

	___navComponent();

	return true;

};

glhf.onready( function ()
{

	accountProfile = glhfPage( accountProfile );

	return true;

} );

module.exports = accountProfile;



'use strict';

var express = require('express')
	, router = express.Router();

module.exports = function ( conf )
{

	router.get( '/account/profile', require( './get' ).bind( { models: conf.models } ) );

	// router.post( '/account/profile', require( './post' ).bind( { models: conf.models } ) );

	// router.put( '/account/profile', require( './put' ).bind( { models: conf.models } ) );

	return router;

};
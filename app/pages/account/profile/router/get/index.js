
module.exports = function ( req, res, next )
{

	if ( req.user )
	{

		if ( req.xhr === true )
		{

			return res.json( { location: '/dashboard' } );

		}

		return res.redirect( '/dashboard' );

	}

	else
	{

		if ( req.xhr === true )
		{

			return res.json( { location: '/account/profile', reload: true } );

		}

		return res.render( 'client' );

	}

};


var glhf = require('glhf')
	, glhfPage = require('glhfPage')
	, m = require('mithril')
	, accountCreate =
	{

		identity: 'accountCreate'

		, baseUrl: '/account/create'

		, components: {}

	};

accountCreate.controller = function ()
{

	var data = {};


	return {};

};

accountCreate.view = function ( ctrl )
{

	var view=[];

	return view;

};

accountCreate.init = function ()
{

	glhf.canvas.innerHTML = glhf.views.page.accountCreate();

};

glhf.onready( function ()
{

	accountCreate = glhfPage( accountCreate );

	// m.route( glhf.canvas, '/', { '/auth/accountCreate': accountCreate } );

});

module.exports = accountCreate;


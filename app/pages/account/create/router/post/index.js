
var _=require('lodash');

module.exports = function ( req, res, next )
{

	if ( req.user )
	{

		if ( req.xhr === true ) return res.json( { location: '/dashboard' } );

		return res.redirect( '/dashboard' );

	}

	else
	{

		return this.models.users.create( req.body, function ( err, user )
		{

			if ( req.xhr === true )
			{

				if (err)
				{

					res.status(400);

					return res.json(err);

				}

				else
				{

					return req.login( user, function ( err )
					{

						if (err)
						{

							res.status(500);

							return res.json(err);

						}

						return res.json( { location: '/account/profile', user: user } );

					} );

				}

			}

			else
			{

				if (err)
				{

					res.locals.message=err.message;

					return res.render('error');

				}

				return res.redirect( '/account/profile' );

			}

		} );

	}

};

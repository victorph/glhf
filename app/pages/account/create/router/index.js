
'use strict';

var express = require('express')
	, router = express.Router();

module.exports = function ( conf )
{

	router.get( '/account/create', require( './get' ).bind( { models: conf.models } ) );

	router.post( '/account/create', require( './post' ).bind( { models: conf.models } ) );

	return router;

};
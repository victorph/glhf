
[Lodash]: https://lodash.com/docs
[Mithril]: https://lhorie.github.io/mithril/mithril.html
[Async]: https://github.com/caolan/async
[Waterline]: https://github.com/balderdashy/waterline-docs
[WaterlineSchema]: https://github.com/balderdashy/waterline-schema
[Browserify]: http://github.com/substack/node-browserify
[Mingo]: https://github.com/kofrasa/mingo
[Express]: http://expressjs.com/api.html
[Karma]: https://www.npmjs.com/package/karma
[Grunt]: gruntjs.com/getting-started
[Mocha]: https://mochajs.org
[Chai]: http://chaijs.com
[MithrilPromises]: http://mithril.js.org/mithril.deferred.html
[Passport]: http://passportjs.org
[Nodemon]: https://github.com/remy/nodemon#nodemon
[Jade]: http://jade-lang.com/
[Moment]: http://momentjs.com/docs/
[Chance]: http://chancejs.com
[ws]: https://travis-ci.org/websockets/ws

[glhf]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhf.js?at=master "glhf"

[glhfCache]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfCache.js?at=master "glhfCache"

[glhfStorage]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfStorage.js?at=master "glhfStorage"

[glhfPage]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfPage.js?at=master "glhfPage"

[glhfComponent]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfComponent.js?at=master "glhfComponent"

[glhfRouter]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfRouter.js?at=master "glhfRouter"

[glhfSocket]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfSocket.js?at=master "glhfSocket"

[glhfCacheTest]: https://bitbucket.org/victorph/glhf/src/b9f8431b06e76224725e92342a3c5791cd3c9a2f/glhf/tests/glhfCache.test.js?at=master&fileviewer=file-view-default "glhfCacheTest"

[glhfXHR]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfXHR.js?at=master "glhfXHR"

[glhfXHRTransports]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfXHRTransports.js?at=master "glhfXHRTransports"

[glhfDataAdapater]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfDataAdapater.js?at=master "glhfDataAdapater"

[glhfCollection]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfCollection.js?at=master "glhfCollection"

[glhfCollectionModel]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfCollectionModel.js?at=master&fileviewer=file-view-default "glhfCollection.Model"

[glhfCollectionQuery]: https://bitbucket.org/victorph/glhf/src/d19fe88a085b5d55ea92a3451bfc548294c2c26f/glhf/lib/glhfCollection.js?at=master&fileviewer=file-view-default#glhfCollection.js-130 "glhfCollection.Query"

# GL HF !

### *From the expression gl hf in the video game community, good luck, have fun.*

# Directory structure:

| Path | Role |
| ------------- | ------------- |
| /glhf | glhf main folder, libs, source, etc. |
| /glhf/lib | The glhf libs source code. |
| [/glhf/lib/glhf.js][glhf] | The main glhf script that holds most of the application data. |
| [/glhf/lib/glhfCache.js][glhfCache] | glhf Cache system. |
| [/glhf/lib/glhfStorage.js][glhfStorage] | glhf Storage system. |
| [/glhf/lib/glhfPage.js][glhfPage] | glhf Page factory. |
| [/glhf/lib/glhfComponent.js][glhfComponent] | glhf Component factory. |
| [/glhf/lib/glhfRouter.js][glhfRouter] | glhf Routing system. |
| [/glhf/lib/glhfSocket.js][glhfSocket] | glhf WebSocket Library. |
| [/glhf/lib/glhfCollection.js][glhfCollection] | glhf Collection class. |
| [/glhf/lib/glhfCollectionModel.js][glhfCollectionModel] | glhf Collection.Model class. |
| [/glhf/lib/glhfDataAdapater.js][glhfDataAdapater] | glhf Data Adapter class. The backbone of glhfCollection, Joining glhfCache, glhfXHR, and glhfSocket. |
| [/glhf/lib/glhfXHR.js][glhfXHR] | glhf XHR class, Handles all data requests to the servers. |
| [/glhf/lib/glhfXHRTransports.js][glhfXHRTransports] | glhf XHR transports class, Has definitions for the multiple data transports for XHR |
| /glhf/models | glhf application models. |
| /glhf/tests | glhf programmatic tests. |
| [/glhf/tests/glhfCache.test.js][glhfCacheTest] | Cache's programmatic tests |
| /app | Application's main path. |
| /app/pages | The application pages. |
| /app/models | The application models. |
| /app/components | The application components. |
| /app/views | The application views. |
| /app/routes | The application web routes. |
| /app/middleware | The application web middle-ware. |
| /app/roles | The application web roles. |
| /api/index.js | Primary API library for all Collections. |
| /config/db.js | Database start-up and configuration file. |
| /public | Static files (css, js, images, etc.) that are publicly available. |
| /app.js | App main loader. |
| /appWS.js | App WebSocket server and main loader. |
| /Gruntfile.js | Projects Gruntfile holding all operations to compile, test, and watch the project. |
| /package.json | Projects configuration for npm. (Hold information related to the application back end behavior.). |
| /bower.json | Projects configuration for bower. (Holds the info of what packages should be installed for the front.). |
| /karma.conf.js | Projects Karma configuration file for the programmatic tests. |
| /.env | Projects environmental file, Holding vars to change behavior between development and production. |
| /.jshintrc | Projects .jshintrc configuration file for JSLint |
| /.gitignore | Projects .gitignore |

# Development startup

### Start the web server. (The Express server will auto restart on the edit of back end files.).

	npm start


## _Note You need to have installed [Nodemon][] for this to work properly:_

	npm i -g nodemon

***

#### Concurrent compile of glhf and its assets. *(Regenerates all jade, scripts, and less)*

	grunt watch:app

***

#### Concurrent test of glhf. *(This is not needed for css/less editing)*

	grunt karma:app

***

# [glhfCollection][] *(Front end)*

All [glhfCollection][] paginate by default, In both the cache, And when it requests the API.

Any [glhfCollection][] object has [Mingo][] loaded in [Collection.Query()][glhfCollectionQuery].

## Pagination

	var User = new glhfCollection( model );

	User.find();

	// Calls the stateless paginate object, This function calculates pages on real time based on the Collection
	// dataset, And also does the same for the slices.

	User.paginate();

	// if the collection contains data
	// Object {slice: Array[3], page: Object}
	// if not
	// Object {slice: Array[0], page: Object}

	// Goes to the collections next page

	User.paginate().nextPage();

	// Goes to the collections next page

	User.paginate().prevPage();

## REST like operations.

	var User = new glhfCollection( model );

	// Create. Note: this wont create the docs in the DB remotely only locally.

	User.create( { email: 'yo@victorph.com'  } );

	// Save Note: This saves remotely and locally.

	User.save( model, data, function( err, docs ){ } );

	// Delete to server

	User.delete( model, function( err, docs ){ } );

	// Fetch all from server.

	User.find();

	// Fetch from server new data.

	User.find( { age: { $lte: 50 } } );

## MongoDB style of query

	// Query locally for data.

	User.Query( { age: { $lte: 50 } } );

	// Fetch one, long or short method

	var query = User.Query( { id: "id" } );

	var query = User.Query( "id" );

***

# [Collection.Model][glhfCollectionModel] (Front end)

Most methods should work fine with both [Async][] and [mithril promises][MithrilPromises].

*Meaning that this works:*

	var user = User.create( newuser );

*And also this works:*

	User.create( newuser, function ( err, doc )
	{

		console.log( err, doc );

	} );

Models have the basic events: changed, updated, deleted.

	user.on( 'changed', function ( newvalue, oldvalue, doc )
	{

		console.log( newvalue, oldvalue, doc );

	});

	// You can hook the event directly to the path you want, If you do not need to check all paths of a model.

	user.on( 'changed:status', function ( newvalue, oldvalue, doc )
	{

		console.log( newvalue, oldvalue, doc );

	});

### Examples

Any [Collection][glhfCollection] object has [Mingo][Mingo] loaded in [Collection.Query()][glhfCollectionQuery].

So far the [Collection.Model][glhfCollectionModel] use a schema like the [Waterline][Waterline] model [schema][WaterlineSchema].

```javascript

// create only works as a sync function.

var user = User.Model( { email: 'bl@blue.com' } );

// autosave can be turned on in the collection or in each model.

user.autosave = true;

// needSave returns true if doc has been modified and not saved.

user.needSave;

// save works with async and promises

user.save();

// delete works with async and promises

user.delete();

// Model events

user.on( 'saved', function ( doc )
{

	console.log(doc);

});

user.on( 'deleted', function ( doc )
{

	console.log(doc);

});

user.on( 'error', function ( doc )
{

	console.log(doc);

});

user.on( 'changed', function ( newvalue, oldvalue, doc )
{

	console.log(doc);

});

user.on( 'changed:status', function ( newvalue, oldvalue, doc )
{

	console.log(doc);

});

```

***

# glhf Generators: yeoman #

#### Progress in the generators: ####

##### How to install: #####

	npm i generator-glhf

##### How to use: #####

	yo glhf:page name // name can be about or user/profile
		// generates depending on the input:
		// app/pages/puser/profile/index.js
		// app/pages/puser/profile/model/index.js
		// app/pages/puser/profile/front/index.js
		// etc...

	yo glhf:component name // name can be userCard but nor user/Card
		// generates depending on the input:
		// app/components/userCard/index.js
		// etc...

	yo glhf:model name // name can be userModel but nor user/Model
		// generates depending on the input:
		// app/components/**/model/index.js
		// app/pages/**/model/index.js

***

###### There are a plenty of features missing in the documentation

***

# This project is primarily built using the next node modules:

[Lodash][Lodash], [Async][Async], [Grunt][Grunt], [Waterline][Waterline], [Browserify][Browserify], [Express][Express],
[Mithril][Mithril], [Mingo][Mingo], [Passport][Passport], [Jade][Jade], [Moment][Moment], [Chance][Chance], [WebSockets][ws],
[Nodemon][]

# And this node modules to do BDD.

[Karma][Karma], [Mocha][Mocha], [Chai][Chai]

***

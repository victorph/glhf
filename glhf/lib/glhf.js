
"use strict";

var _ = require('lodash')
	, async = require('async');

var glhf =
{

	env: process.env.NODE_ENV

	, canvas: undefined

	, router: undefined

	, ws:
	{
		url: 'http://localhost:3000/'
	}

	, conf:
	{

		transport: 'jquery'

	}

	// TODO: HTML injection point.
	, target: 'mainApp'

	, views: { page: {}, component: {}, app: {} }

	, pages: {}

	, components: {}

	, routes: {}

	, models:
	{

		components: {}

		, pages: {}

		, client: {}

		, db:
		{

			user: require( 'models/users.js' )

			, role: require( 'models/role.js' )

		}

	}

};

Object.defineProperty( glhf, '_onready', { writable: true , value: [] } );

Object.defineProperty( glhf, 'identity', { enumerable: true , value: 'glhf' } );

Object.defineProperty( glhf, 'onready',
{

	value: function ( fn )
	{

		if ( _.isFunction( fn ) )
		{
			return glhf._onready.push( fn );
		}

		return false;

	}

} );


glhf.init = function ( ev )
{

	glhf.canvas = document.getElementById( glhf.target );

	glhf.router = require('glhfRouter')();

	var _logic={};

	_logic.start=function(done)
	{

		async.each( glhf._onready, function ( fn, cb )
		{

			fn( ev );

			cb();

		}, function ( err )
		{

			if (err)
			{
				console.error(err);
				throw err;
			}

			glhf._onready=[];

			done();

			return true;

		} );

	};

	_logic.router = [ 'start', glhf.router.init ];

	async.auto( _logic, function ( err, res )
	{

		if ( glhf.env === 'development' )
		{

			console.log( 'glhf.env', glhf.env );

			console.log( 'starting page', res.router );

		}

	} );

};

if ( typeof global.document !== 'undefined' )
{

	global.addEventListener( 'load',  glhf.init );

	window.addEventListener( 'unload', function ()
	{

		console.log('going out');

	});

	window.addEventListener( 'offline', function ()
	{

		console.log('We are offline!');

	});

	window.addEventListener( 'online', function ()
	{

		console.log('We are online!');

	});

}

module.exports = glhf;

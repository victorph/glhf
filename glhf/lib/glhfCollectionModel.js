
'use strict';

/* global EventEmitter2:true */

var _ = require('lodash')
	// , async = require('async')
	, glhf = require('glhf')
	, moment = require('moment')
	, EventEmitter2 = require('eventemitter2').EventEmitter2
	, Model;

Model = function ( conf )
{

	var parent = this;

	var glhfModel = function ()
	{

		var ee = new EventEmitter2()
			, obj =
			{

				values: { value: {} , writable: true }

				, autosave: { value: false , writable: true }

				, deleted: { value: false , writable: true }

				, on: { value: ee.on , writable: false }

				, emit: { value: ee.emit , writable: false }

				, _events: { value: {} , writable: true }

				, createdAt: { value: moment( _.now() ), writable: true, enumerable: true }

				, updatedAt: { value: moment( _.now() ), writable: true, enumerable: true }

				// TODO
				// this needs to reflect the models toJSON, not this one.

				, toJSON:
				{

					value: function ()
					{

						var res = _.cloneDeep( this.values );

						res.id = this.id; return res;

					}

					, writable: true

				}

			};

		if ( _.isUndefined( conf.id ) )
		{

			obj.id = { value: undefined , writable: true , enumerable: true , configurable: true };

			obj.needSave = { value: true , writable: true };

			obj.key = { value: 'GLHF_COLL' + parent.identity.toUpperCase() + '_MODEL' };

		}

		else
		{

			obj.id = { value: conf.id , enumerable: true };

			obj.needSave = { value: false , writable: true };

			obj.key = { value: 'GLHF_COLL' + parent.identity.toUpperCase() + '_MODEL' + conf.id };

		}

		if ( _.isBoolean( parent.autosave ) && parent.autosave === true )
		{

			obj.autosave = { value: true , writable: true };

		}

		var self = Object.create( null, obj );

		_.each( parent.attributes, function ( val, key )
		{

			var getset =
			{

				get: function ()
				{

					return self.values[ key ];

				}

				, set: function ( val )
				{

					ee.emit( 'changed', key, self.values[ key ], val, self.id );

					ee.emit( 'changed:' + key, self.values[ key ], val, self.id );

					self.values[ key ] = val;

					return val;

				}

			};

			if ( key === 'id' )
			{

				self.values[ key ] = conf[ key ];

				delete getset.set;

			}

			Object.defineProperty( self, key, getset );

		} );

		_.each( conf, function ( val, key )
		{

			if ( key === 'updatedAt' || key === 'createdAt' )
			{

				val = moment( val );

			}

			self[ key ] = val;

		} );

		self.save = function ( callback )
		{

			return parent.save( self, function ( err, docs )
			{

				docs = docs[ 0 ];

				if ( _.isEmpty( err ) && ! _.isUndefined( docs ) )
				{

					if ( self.needSave === true )
					{

						self.needSave = false;

					}

					ee.emit( 'saved', self.id );

				}

				else
				{

					ee.emit( 'error', err );

				}

				if ( _.isFunction( callback ) )
				{

					return callback.apply( self, [ err, docs ] );

				}

			} );

		};

		self.delete = function ( data, callback )
		{

			if ( _.isUndefined( self.id ) )
			{
				return;
			}

			return parent.delete( self, function ( err, docs )
			{

				if ( _.isEmpty( err ) )
				{

					if ( _.size( docs ) > 0 )
					{

						self.id	= undefined;
						self.deleted = true;
						self.needSave = false;

						ee.emit( 'deleted', self.id );

					}

				}

				else
				{

					ee.emit( 'error', err, self.id );

				}

				if ( _.isFunction( callback ) )
				{
					return callback.apply( self, [ err, docs ] );
				}

			} );

		};

		Object.observe( self.values, function ( changes )
		{

			var schema = _.findWhere( glhf.models.db, { identity: parent.identity } )
				, allowed = _.get( schema, 'conf.allowed.put', undefined ); // only put???

			if ( ! _.isUndefined( allowed ) && _.isArray( allowed ) )
			{

				var allow = false;

				_.each( changes, function ( item )
				{

					if ( _.includes( allowed, item.name ) )
					{
						allow = true;
					}

				} );

				if ( allow === true )
				{

					self.needSave = true;
					self.updatedAt = moment( _.now() );

				}

			}

			if ( self.autosave === true && self.needSave === true )
			{

				var _sav=false;

				if ( ! _.isUndefined( allowed ) )
				{

					_.each( changes, function ( item )
					{

						if ( _.includes( allowed, item.name ) )
						{
							_sav = true;
						}

					} );

					if ( _sav === true )
					{

						self.save( function ( err, res )
						{

							// retry if save fails or what?

							console.log( 'collectionModel autosave', err, res );

						} );

					}

				}

			}

			if ( glhf.env === 'development' )
			{
				console.log( 'Model.values > changes', changes );
			}

		} );

		return self;

	};

	return new glhfModel();

};

module.exports = Model;

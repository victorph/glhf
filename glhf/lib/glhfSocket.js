
"use strict";

var _ = require('lodash')
	, io = require('socket.io-client');
	// , async = require('async')
	// , glhf = require('glhf');

window.glhfSocket = function glhfSocket ( conf )
{

	function socketObject ( skt )
	{

		var self = Object.create( null,
		{

			socket:
			{
				value: skt
			}

			, on:
			{

				value: function on ( path, cb )
				{

					skt.on( path, cb );

				}

			}

			, emit:
			{

				value: function emit ( path, data, cb )
				{

					if ( _.isFunction( cb ) )
					{
						return self.socket.emit( path, data, cb );
					}

					return self.socket.emit( path, data );

				}

			}

			, join:
			{

				value: function join ( chan )
				{

					self.socket.join( chan );

				}

			}

			, leave:
			{

				value: function leave ( chan )
				{

					self.socket.leave( chan );

				}

			}

		} );

		return self;

	}

	var target = '//' + window.location.host;

	if ( _.isPlainObject( conf ) )
	{

		if ( _.isString( conf.target ) )
		{

			target += '/' + conf.target;

		}

		if ( _.isString( conf.port ) || _.isInteger( conf.port ) )
		{

			target += ':' + conf.port;

		}

	}

	var socket = io.connect( target );

	// socket.on( 'connect', function ( socket )
	socket.on( 'connect', function ()
	{

		console.log( 'socket connect', arguments );

	} );

	socket.on( 'disconnect', function ()
	{


		console.log( 'socket disconnect', arguments );


	} );

	socket.on( 'error', function ()
	{


		console.log( 'socket error', arguments );


	} );

	socket.on( 'reconnect', function ()
	{


		console.log( 'socket reconnect', arguments );


	} );

	socket.on( 'reconnecting', function ()
	{


		console.log( 'socket reconnecting', arguments );


	} );

	socket.on( 'reconnect_attempt', function ()
	{


		console.log( 'socket reconnect_attempt', arguments );


	} );

	socket.on( 'reconnect_failed', function ()
	{


		console.log( 'socket reconnect_failed', arguments );


	} );

	socket.on( 'reconnect_error', function ()
	{


		console.log( 'socket reconnect_error', arguments );


	} );

	return new socketObject( socket );

};

module.exports = window.glhfSocket;


'use strict';

/* global Mingo:true */

var _ = require('lodash')
	// , async = require('async')
	, EventEmitter2 = require('eventemitter2').EventEmitter2
	, glhf = require( 'glhf' )
	, glhfDataAdapter = require( 'glhfDataAdapter' )
	, glhfCollectionModel = require( 'glhfCollectionModel' );

var glhfCollection = function glhfCollection ( opts, owner )
{

	var Collection = function glhfCollection ()
	{

		var ee = new EventEmitter2()
			, self = Object.create( null, {} )
			, current_page = 1
			, adapter
			, obj =
			{

				identity:
				{

					value: opts.identity

					, enumerable: true

				}

				, conf:
				{

					value: opts.conf

					, writable: true

					, enumerable: true

				}

				, attributes:
				{

					value: opts.attributes

					, writable: true

					, enumerable: true

				}

				, baseUrl:
				{

					value: opts.baseUrl

					, writable: true

					, enumerable: true

				}

				, on:
				{

					value: ee.on

				}

				, emit:
				{

					value: ee.emit

				}

				, beforeCreate:
				{
					value: opts.beforeCreate ? opts.beforeCreate : null
				}

				, key:
				{

					value: 'GLHF_COLL' + opts.identity.toUpperCase()

					, enumerable: true

				}

			};

		Object.defineProperties( self, obj );

		adapter = new glhfDataAdapter( self );

		if ( ! _.isUndefined( owner ) )
		{

			Object.defineProperty( self, 'owner', { value: owner } );

			var _owner = glhf[ owner.type ][ owner.identity ];

		}

		Object.defineProperty( self, 'adapter', { value: adapter } );

		Object.defineProperty( self, 'dataset', { get: function () { return adapter.dataset; } } );

		Object.defineProperty( self, 'paginate', {

			value: function ()
			{

				var self = this
					, out = Object.create( function paginate () {}, {} )
					, _slice = []
					, last = 0
					, page = Object.create( null, { } );

				Object.defineProperty( out, 'current', { enumerable: false, writable: true, value: current_page } );

				Object.defineProperty( page, 'start', { enumerable: false, writable: true, value: ( current_page - 1 ) * self.conf.paginate.limit } );

				Object.defineProperty( page, 'limit', { enumerable: false, writable: true, value: self.conf.paginate.limit  } );

				var data = _.map(self.adapter.cache.memory.storage,function(i){return i;});

				_slice = _.slice( data, page.start, page.limit + page.start );

				if ( _.size( _slice ) < page.limit )
				{

					last = current_page;

				}

				Object.defineProperty( page, 'last', { enumerable: false, writable: true, value: last } );

				Object.defineProperty( out, 'slice', { enumerable: true, writable: true, value: _slice } );

				Object.defineProperty( out, 'page', { enumerable: true, writable: true, value: page } );

				Object.defineProperty( out, 'nextPage',
				{

					value: function ()
					{

						current_page += 1;

						var c = current_page;

						if ( last !== 0 && c >= last )
						{

							current_page = last;

						}

						_owner.view( _owner.controller() );

						return c;

					}

				} );

				Object.defineProperty( out, 'prevPage',
				{

					value: function ()
					{

						current_page -= 1;

						var c = current_page;

						if ( c <= 1 )
						{

							current_page = 1;

						}

						_owner.view( _owner.controller() );

						return c;

					}

				} );

				return out;

			}

		} );

		Object.defineProperty( self, 'Query', {

			value: function ( what, ds )
			{

				Mingo.setup( { key: 'id' } );

				if ( _.isUndefined( ds ) )
				{
					ds = _.map( self.adapter.cache.memory.storage, function ( i ) { return i; } );
				}

				if ( _.isEmpty( ds ) )
				{
					return;
				}

				if ( _.isString( what ) )
				{
					what = { id: what };
				}

				var query = new Mingo.Query( what ).find( ds );

				return query;

			}

		} );

		Object.defineProperty( self, 'Aggregate', {

			value: function ( what, ds )
			{

				Mingo.setup( { key: 'id' } );

				if ( _.isUndefined( ds ) )
				{
					ds = _.map( self.adapter.cache.memory.storage, function ( i ) { return i; } );
				}

				if ( _.isEmpty( ds ) )
				{
					return;
				}

				if ( _.isString( what ) )
				{
					throw new Error('what must be an Array');
				}

				var agg = new Mingo.Aggregator( what );

				return agg.run( ds );

			}

		} );

		self.count = adapter.count;

		self.create = adapter.create;

		self.find = adapter.find;

		self.save = adapter.save;

		self.delete = adapter.delete;

		self.Model = glhfCollectionModel;

		return self;

	};

	return new Collection();

};

module.exports = glhfCollection;

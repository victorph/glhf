
'use strict';

var _ = require('lodash')
	// , async = require('async')
	, glhf = require('glhf')
	// , moment = require('moment')
	, glhfCollection = require('glhfCollection');
	// , glhfComponent = require('glhfComponent');

function glhfPage ( model )
{

	function pageObject ()
	{

		if ( _.isUndefined( model ) )
		{
			throw new Error( 'glhfPage error no options...' );
		}

		if ( _.isUndefined( model.baseUrl ) )
		{
			throw new Error( 'glhfPage error no baseUrl...' );
		}

		if ( _.isUndefined( model.view ) )
		{
			throw new Error( 'glhfPage error no view...' );
		}

		if ( _.isUndefined( model.identity ) )
		{
			throw new Error( 'glhfPage error no identity...' );
		}

		var self = Object.create( null, {} );

		Object.defineProperty( self, 'init', { enumerable: false, configurable: false, writable: false, value: model.init } );

		Object.defineProperty( self, 'baseUrl', { enumerable: true, configurable: false, writable: false, value: model.baseUrl } );

		Object.defineProperty( self, 'components', { enumerable: true, configurable: false, writable: true, value: model.components } );

		Object.defineProperty( self, 'channels', { enumerable: true, configurable: false, writable: false, value: model.channels } );

		Object.defineProperty( self, 'controller', { enumerable: false, configurable: false, writable: false, value: model.controller } );

		Object.defineProperty( self, 'view', { enumerable: false, configurable: false, writable: false, value: model.view } );

		_.each( self.collections, function ( v, k )
		{

			v = _.get( window, v, undefined );

			if ( ! _.isUndefined( v ) && _.isUndefined( self.collections[ k ] ) )
			{

				self.collections[ k ] = new glhfCollection( v, { type: 'pages', identity: model.identity } );

			}

		} );

		glhf.routes[ model.baseUrl ] = glhf.pages[ model.identity ] = self;

		return self;

	}

	return new pageObject();

}

module.exports = glhfPage;

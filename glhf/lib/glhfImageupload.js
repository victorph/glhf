
"use strict";

/*global glhf:true*/
/*global $:true*/
/*global _:true*/
/*global Dropzone:true*/

var glhf = require('glhf');

glhf.imageUpload = function overloadImageUpload ( params, callback )
{

	var _msg
		, _results = []
		, _$check = $( params.targetElement );

	if ( _$check.length === 0 )
	{

		_msg = 'Target element could not be found';

		console.error( _msg );

		if ( _.isFunction( callback ) )
		{
			return callback( true, _msg );
		}

		return;

	}

	var _opts =
		{

			url: 'vision.centroculturadigital.mx/image'

			, paramName: 'image'

			, maxFilesize: 2

			, parallelUploads: 100

			, uploadMultiple: true

			, headers:
			{
				// 'os': $.cookie('overload.session')
			}

			, thumbnailWidth: 22
			, thumbnailHeight: 22

			, addRemoveLinks: true
			, dictCancelUpload: ''
			, dictRemoveFile: "x"

		};

	if ( _.isString( params.url ) )
	{
		_opts.url = params.url;
	}

	if ( _.isString( params.paramName ) )
	{
		_opts.paramName = params.paramName;
	}

	if ( ! _.isUndefined( params.clickable ) )
	{
		_opts.clickable = params.clickable;
	}

	if ( _.isString( params.maxFilesize ) )
	{
		_opts.maxFilesize = params.maxFilesize;
	}

	if ( ! _.isUndefined( params.autoProcessQueue ) )
	{
		_opts.autoProcessQueue = params.autoProcessQueue;
	}

	if ( ! _.isUndefined( params.headers ) )
	{
		_opts.headers = _.assign( _opts.headers, params.headers );
	}

	if ( ! _.isUndefined( params.previewsContainer ) )
	{
		_opts.previewsContainer = params.previewsContainer;
	}

	var $progress
		, _imageUpload = new Dropzone( params.targetElement, _opts );

	if ( _.isString( params.progress ) )
	{
		$progress = params.progress;
	}

	else
	{
		$progress = $( '.progress-system' );
		if ( _.size( $progress ) === 0 )
		{
			$progress = undefined;
		}
	}

	// _imageUpload.on( 'sending', function onSending ( file, xhr, formData ) {

	_imageUpload.on( 'sending', function onSending ()
	{

		if ( ! _.isUndefined( $progress ) )
		{
			$progress.stop( true, false ).animate( { opacity: 1, width: '100%' }, 560 );
		}

		if ( ! _.isUndefined( params.event ) && _.isFunction( params.event.sending ) )
		{
			params.event.sending();
		}

		return;

	} );

	// _imageUpload.on('uploadprogress',function(file,progress,size){

	// _imageUpload.on( 'uploadprogress', function onUploadProgress ( file, progress, size ) {
		// $progress.attr('aria-valuenow',progress);
		// $progress.width(progress+'%');
		// console.log(file,progress,size);
	// });

	// _imageUpload.on('thumbnail',function(f,d){
	// 	$('#feedEditorThumbs').append('<img src="'+d+'">');
	// });

	// _imageUpload.on('maxfilesexceeded',function(file){
		// console.log(_imageUpload.files.reverse().pop());
		// _imageUpload.removeFile(file);
	// });

	// _imageUpload.on('removedfile',function(file){
	// });

	// _imageUpload.on("addedfile",function(file){

	_imageUpload.on( "addedfile", function onAddedFile () {

		// if ( _imageUpload.files.length>_opts.maxFiles) _imageUpload.removeFile(file);

		if ( ! _.isUndefined( params.body ) )
		{

			_imageUpload.options.headers.body = {};
			_imageUpload.options.headers.body = JSON.stringify(params.body);

		}

		if ( ! _.isUndefined( params.headers.target ) )
		{

			_imageUpload.options.headers.target = {};
			_imageUpload.options.headers.target = JSON.stringify(params.headers.target);

		}

		if ( ! _.isUndefined( params.headers.gallery ) )
		{

			_imageUpload.options.headers.gallery = {};
			_imageUpload.options.headers.gallery = JSON.stringify(params.headers.gallery);

		}

		if ( ! _.isUndefined( params.event ) && _.isFunction( params.event.addedFile ) )
		{
			params.event.addedFile();
		}

		return;

	} );

	_imageUpload.on( "successmultiple", function onSuccessMultiple ( file, response ) {

		response = JSON.parse( response );

		if ( ! _.isUndefined( params.event ) && _.isFunction( params.event.success ) )
		{
			params.event.success();
		}

		if ( _.isEmpty( response.error ) && _.isString( response.url ) )
		{

			if ( ! _.isUndefined( $progress ) )
			{
				$progress.addClass( 'progress-bar-success' );
			}

			_results.push( file );

			if ( _.isFunction( callback ) )
			{
				return callback( null, response );
			}

		}

		else
		{

			if ( ! _.isUndefined( $progress ) )
			{
				$progress.addClass( 'progress-bar-error' );
			}

			if ( _.isFunction( callback ) )
			{
				return callback( true, response );
			}

		}

	} );

	// _imageUpload.on("completemultiple", function onCompleteMultiple ( file ) {

	_imageUpload.on( "completemultiple", function onCompleteMultiple ( ) {

		if ( ! _.isUndefined( $progress ) )
		{

			$progress
			.stop( true, false )
			.animate( { opacity: 0 }, 860 );

		}

		if ( ! _.isUndefined( params.event ) && _.isFunction( params.event.complete ) )
		{
			params.event.complete();
		}

		_imageUpload.removeAllFiles();

		return;

	} );

	_imageUpload.on( "error", function onError ( file ) {

		if ( ! _.isUndefined( params.event ) && _.isFunction( params.event.error ) )
		{
			params.event.error();
		}

		if ( ! _.isUndefined( $progress ) )
		{

			$progress.addClass( 'progress-bar-error' );

			$progress
			.stop( true, false )
			.animate( { opacity: 0 }, 860, function () {

				$progress.removeClass( 'progress-bar-error' );

				if ( _.isFunction( callback ) )
				{
					return callback( true, file );
				}

			} );

		}

		else
		{

			console.error( 'Image upload failed for:', file );

			if ( _.isFunction( callback ) )
			{
				return callback( true, file );
			}

		}

	} );

	return _imageUpload;

};

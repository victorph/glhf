
'use strict';

var _ = require( 'lodash' )
	, async = require( 'async' )
	, m = require( 'mithril' )
	, glhfStorage = require( 'glhfStorage' )
	, moment = require( 'moment' );

function glhfCacheSync ( data, callback )
{

	var _err
		, self = Object.create( null, {} )
		, all = {}
		, deferred = m.deferred()
		, tasks = {};

	if ( _.isUndefined( data ) )
	{
		_err = new Error('data is undefined');
	}

	// Catches error and finish the operation.

	if ( _.isError( _err ) )
	{

		if ( _.isUndefined( callback ) )
		{
			throw _err;
		}

		else
		{
			return callback( _err );
		}

	}

	if ( self.type === glhfStorage.IN_MEMORY_STORAGE )
	{
		all = self.storage;
	}

	tasks.sync = function sync ( done )
	{

		var synced=[]
			, needSave=[];

		if ( self.type === glhfStorage.IN_MEMORY_STORAGE )
		{

			_.each( data, function ( doc )
			{

				var target = self.storage[ doc.id ];

				if ( _.isObject( target ) )
				{

					if ( target.updatedAt.unix() < doc.updatedAt.unix() )
					{
						self.storage[ doc.id ] = doc;
					}

					else
					{
						doc = target;
					}

					synced.push( doc );

					if ( target.needSave === true )
					{
						needSave.push( target );
					}

				}

			} );

		}

		return done( null, { synced: synced, needSave: needSave } );

	};

	async.auto( tasks, function ( err, results )
	{

		if ( process.env.NODE_ENV === 'development' )
		{
			console.log( 'glhfCacheSync', err, results );
		}

		if ( _.isFunction( callback ) )
		{
			return callback( err, results.sync );
		}

		return deferred.resolve( results.sync );

	});

	return deferred.promise;

}

function __glhfCacheCompare ( itemA, itemB )
{

	var out;

	if ( itemA.id === itemB.id )
	{

		var dateA = moment( itemA.updatedAt )
			, dateB = moment( itemB.updatedAt );

		out = dateA.diff( dateB, 'minutes' );

		if ( out !== 0 )
		{
			out = [ itemA, itemB, out ];
		}

	}

	return out;

}

function glhfCacheCompare ( data, callback )
{

	var tasks = {}
		, deferred = m.deferred();

	tasks.compare = function ( done )
	{

		var out = [];

		async.each( data.all, function ( item, nxt )
		{

			var comp;

			if ( _.isArray( data.with ) )
			{

				_.each( data.with, function ( subi )
				{

					comp = __glhfCacheCompare( item, subi );

					if ( ! _.isEmpty( comp ) )
					{
						out.push( comp );
					}

				} );

			}

			else
			{

				comp = __glhfCacheCompare( item, data.with );

				if ( ! _.isEmpty( comp ) )
				{
					out.push( comp );
				}

			}

			nxt();

		}, function ()
		{

			return done( null, out );

		} );

	};

	async.auto( tasks, function ( err, results )
	{

		if ( _.isFunction( callback ) )
		{
			return callback( err, results.compare );
		}

		return deferred.resolve( results.compare || err );

	} );

	return deferred.promise;

}

var glhfCache = function glhfCache ( conf )
{

	var self = Object.create( null, {} )
		, db
		, storage = conf.storage || glhfStorage.SESSION_STORAGE;

	db = glhfStorage.create( conf.owner, storage );

	Object.defineProperty( self, 'owner', { value: conf.owner } );

	Object.defineProperty( self, 'type', { value: storage } );

	Object.defineProperty( self, 'stamp_key', { value: conf.owner + '_STAMP' } );

	Object.defineProperty( self, 'get', { value: function( key ) { return db.get( key ); } } );

	Object.defineProperty( self, 'set', { value: function( key, val ) { return db.set( key, val ); } } );

	Object.defineProperty( self, 'remove', { value: function( key ) { return db.remove( key ); } } );

	Object.defineProperty( self, 'sync', { value: glhfCacheSync } );

	Object.defineProperty( self, 'compare', { value: glhfCacheCompare } );

	if ( storage !== glhfStorage.IN_MEMORY_STORAGE )
	{

		Object.defineProperty( self, 'memory',
		{
			writable: false

			, value: new glhfCache( {

				owner: conf.owner + 'MEM'

				, storage: glhfStorage.IN_MEMORY_STORAGE

			} )

		} );

	}

	else
	{

		Object.defineProperty( self, 'storage', { writeable: true, value: db.$storage } );

	}

	Object.defineProperty( self, 'updatedAt',
	{

		get: function ()
		{

			var self = this
				, diff = 0
				, stamp = self.get( self.stamp_key ) || moment( _.now() );

			if ( ! _.isUndefined( stamp ) )
			{

				var a = moment( stamp )
					, b = moment( _.now() );

				diff = b.diff( a, 'minutes' );

			}

			return diff;

		}

		, set: function ( time )
		{

			var self = this;

			if ( _.isEmpty( time ) )
			{
				time = moment( _.now() );
			}

			else
			{
				time = moment( time );
			}

			self.set( self.stamp_key, time );

			return time;

		}

	} );

	return self;

};

module.exports = glhfCache;

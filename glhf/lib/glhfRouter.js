
/* global $:true */

/* global bCrypt:true */

"use strict";

var _ = require('lodash')
	// , async = require('async')
	, glhf = require('glhf')
	, bcrypt = new bCrypt()
	, _allowed_tags = ['input','select','textarea'];

function __processRequest ( $el, _req )
{

	_req = Object.create( function requestObject () {}, {} );

	var _href = $el.attr('href');

	if ( _.isUndefined( _href ) )
	{

		var $form = $el.closest('form');

		_req.url = $form.attr('action');

		_req.method = $form.attr('method');

		if ( _.isUndefined( _req.data ) )
		{

			_req.data = {};

		}

		_.each( _allowed_tags, function ( input )
		{

			_.each( $form.find( input ), function ( item )
			{

				if ( ! _.isEmpty( item.value ) && ! _.isEmpty( item.name ) )
				{

					if ( item.type === 'password' )
					{

						var salt = bcrypt.gensalt(10);

						bcrypt.hashpw( item.value, salt, function ( hash )
						{
							_req.data[ item.name ] = hash;
						});

					}

					else
					{

						_req.data[ item.name ] = item.value;

					}

				}

			} );

		} );

	}

	else
	{

		if( _.isUndefined( _req.title ) )
		{

			_req.title = _.isString( $el.attr('title') ) ? $el.attr('title') : '';

		}

		else
		{

			_req.title = _href;

		}

		_req.url = _href;

	}

	console.log('REQUEST',_req);

	return _req;

}

function glhfRouterFactory ( conf )
{

	function glhfRouter ()
	{

		var self = Object.create( null, {  } )
			, $target = $( '#' + glhf.target );

		function __refresh ()
		{

			$( '.internal' ).off( 'click' ).on( 'click', function ( ev )
			{

				ev.preventDefault();

				var _req = __processRequest( $( this ) );

				_req.success = function( data )
				{

					self.load( data.location, data );

					if ( _.isFunction( conf.success ) )
					{
						conf.success(data);
					}

				};

				_req.error = function()
				{

					console.log( 'arguments', arguments );

					// $target.html( glhf.views.app.error( { message: 'Page not found!' } ) );

					$target.fadeIn( self.fadeTime );

					if ( _.isFunction( conf.error ) )
					{
						conf.error.apply(this,arguments);
					}

				};

				$target.fadeOut( self.fadeTime, function ()
				{

					$.ajax( _req );

					return;

				} );

				return false;

			});

		}

		function __popState ( ev )
		{

			if ( _.isEmpty( ev.state ) )
			{
				return;
			}

			if ( glhf.env === 'development' )
			{
				console.log('state changed!',ev);
			}

			$target.fadeOut( self.fadeTime, function ()
			{

				self.load( ev.state.url, ev );

				return;

			} );

			return;

		}

		function __abort ()
		{

			if ( glhf.env === 'development' )
			{
				console.log('aborted!');
			}

			return;

		}

		function __load ( href, data )
		{

			if ( glhf.env === 'development' )
			{
				console.log( 'load data:', data );
			}

			if ( data.reload === true )
			{
				window.location = href;
			}

			if ( ! _.isUndefined( glhf.routes[ window.location.pathname ].components ) )
			{

				_.each( glhf.routes[ window.location.pathname ].components, function ( comp )
				{

					if ( _.isFunction( comp.off ) )
					{

						comp.off();

					}

				} );

			}

			history.pushState( { href: href || window.location.pathname }, document.title, href || window.location.pathname );

			if ( ! _.isUndefined( glhf.routes[ window.location.pathname ] ) )
			{

				if ( _.isFunction( glhf.routes[ window.location.pathname ].init ) )
				{

					glhf.routes[ window.location.pathname ].init();

				}

			}

			else
			{

				$target.html( glhf.views.app.error( { message: 'Page not found!' } ) );

			}

			$target.fadeIn( self.fadeTime );

			self.refresh();

			return;

		}

		function __init ( cb )
		{

			history.pushState( { href: window.location.pathname }, document.title, window.location.pathname );

			if ( ! _.isUndefined( glhf.routes[ window.location.pathname ] ) )
			{

				if ( _.isFunction( glhf.routes[ window.location.pathname ].init ) )
				{

					glhf.routes[ window.location.pathname ].init();

				}

			}

			else
			{

				console.warn('Could not initialize any pages!');

			}

			// refresh click controls using jQuery.

			__refresh();

			var o =
			{

				pathname: window.location.pathname

				, title: document.title

			};

			if ( _.isFunction( cb ) )
			{
				return cb( null, o );
			}

			return o;

		}

		window.onpopstate = __popState;

		window.onabort = __abort;

		Object.defineProperty( self, 'init', { value: __init } );

		Object.defineProperty( self, 'fadeTime', { writeable: true, value: 1200 } );

		Object.defineProperty( self, 'load', { value: __load } );

		Object.defineProperty( self, 'refresh', { value: __refresh } );

		return self;

	}

	return new glhfRouter();

}

module.exports = glhfRouterFactory;

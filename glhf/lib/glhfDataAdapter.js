
"use strict";

var _ = require('lodash')
	, async = require('async')
	// , moment = require('moment')
	, m = require('mithril')
	, glhf = require('glhf')
	, glhfCache = require('glhfCache')
	, glhfSocket = require('glhfSocket')
	// , glhfStorage = require('glhfStorage')
	, glhfXHR = require('glhfXHR');

// checks docs with needSave.

function glhfCheckSave ()
{

}

// calls cache to verify stamps for expiration, and flags for refresh.

function glhfCheckStamp ()
{

}

// calls the system to see if all objects are in sync since last data action

function glhfCheckSync ()
{

}

// checks if unsaved drafts are awaiting save.

function glhfCheckDrafts ()
{

}

function glhfDataAdapter ( collection )
{

	if ( _.isUndefined( collection ) )
	{
		throw new Error('No collection passed');
	}

	if ( _.isUndefined( collection.identity ) )
	{
		throw new Error('Collection identity is not defined.');
	}

	var _cache = new glhfCache( { owner: collection.identity } )
		, self = Object.create( null, {} )
		, identity = collection.identity + 'adapter' || _.uniqueId( 'Adapter_' );

	Object.defineProperty( self, 'identity', { enumerable: true , value: identity } );

	Object.defineProperty( self, 'request', { value: new glhfXHR( collection.request || glhf.conf ) } );

	if ( ! _.isUndefined( collection.socket ) )
	{
		Object.defineProperty( self, 'socket', { value: new glhfSocket( { name: collection.socket } ) } );
	}

	Object.defineProperty( self, 'cache', { value: _cache } );

	Object.defineProperty( self, 'collection', { value: collection } );

	Object.defineProperty( self, 'conf', { value: _.merge( collection.conf ) } );

	Object.defineProperty( self, 'dataset', { writable: true, value: [] } );

	Object.defineProperty( self, 'drafts',
	{

		enumerable: true

		, writable: true

		, value: []

	} );

	Object.defineProperty( self, 'checkSave',
	{

		value: glhfCheckSave

	} );

	Object.defineProperty( self, 'checkStamp',
	{

		value: glhfCheckStamp

	} );

	Object.defineProperty( self, 'checkSync',
	{

		value: glhfCheckSync

	} );

	Object.defineProperty( self, 'checkDrafts',
	{

		value: glhfCheckDrafts

	} );

	Object.defineProperty( self, 'count',
	{

		value: function adapterCount ( callback )
		{

			var deferred = m.deferred()
				, req =
				{

					url: self.conf.baseUrl + '/count'

					, method: 'get'

				};

			self.request.do( req, function ( err, count )
			{

				if ( _.isEmpty( err ) )
				{


				}

				deferred.resolve( count );

				if ( _.isFunction( callback ) )
				{
					return callback.apply( self, [ err, count ] );
				}

				return count || err;

			} );

			return deferred.promise;

		}

	} );

	Object.defineProperty( self, 'create',
	{

		value: function adapterCreate ( data, callback )
		{

			var ndocs
				, _l = {}
				, deferred = m.deferred();

			_l.create = function ( done )
			{

				if ( _.isArray( data ) )
				{

					ndocs = [];

					async.eachSeries( data, function ( item, next )
					{

						var out = self.collection.Model( item );

						out.save( function ( err, doc )
						{

							out = self.collection.Model( doc );

							self.cache.memory.storage[ out.id ] = out;

							ndocs.push( out );

							next();

						} );

					} , function ()
					{

						return done( null, ndocs );

					} );

				}

				else
				{

					var out = self.collection.Model( data );

					out.save( function ( err, doc )
					{

						out = self.collection.Model( doc );

						self.cache.memory.storage[ out.id ] = out;

						return done( null, out );

					} );

				}

			};

			async.auto( _l, function ( err, results ) {

				if (process.env.NODE_ENV==='development')
				{
					console.log( 'glhfDataAdapter', err, results );
				}

				var owner = glhf[ self.collection.owner.type ][ self.collection.owner.identity ];

				owner.view( owner.controller() );

				if ( _.isFunction( callback ) )
				{
					return callback( null, results.create );
				}

				deferred.resolve( results.create );

			} );

			if ( _.isUndefined( callback ) )
			{
				return deferred.promise;
			}

		}

	} );

	Object.defineProperty( self, 'save',
	{

		value: function adapterSave ( model, callback )
		{

			var deferred = m.deferred()
				, req =
				{

					url: self.conf.baseUrl

					, method: 'post'

					, data: {}

				};

			if ( ! _.isEmpty( model.id ) )
			{

				req.url += '/' + model.id;

				req.method = 'put';

			}

			req.data = model.toJSON();

			self.request.do( req, function ( err, docs )
			{

				var owner = glhf[ self.collection.owner.type ][ self.collection.owner.identity ];

				owner.redraw();

				if ( glhf.env === 'development' )
				{
					console.log( 'Collection.update', err, docs );
				}

				if ( _.isEmpty( err ) )
				{

				}

				deferred.resolve( docs || err );

				if ( _.isFunction( callback ) )
				{
					return callback.apply( self, [ err, docs ] );
				}

			} );

			return deferred.promies;

		}

	} );

	Object.defineProperty( self, 'delete',
	{

		value: function adapterDelete ( model, callback )
		{

			var deferred = m.deferred()
				, req =
				{

					url: self.conf.baseUrl + '/' + model.id

					, method: 'delete'

					, data: {}

				};

			self.request.do( req, function ( err, docs )
			{

				if ( _.isEmpty( err ) )
				{

					if ( _.size( docs ) > 0 )
					{

						delete self.cache.memory.storage[ model.id ];

						var owner = glhf[ self.collection.owner.type ][ self.collection.owner.identity ];

						owner.view( owner.controller() );

					}

				}

				deferred.resolve( docs || err );

				if ( _.isFunction( callback ) )
				{
					return callback.apply( self, [ err, docs ] );
				}

			} );

			return deferred.promise;

		}

	} );

	Object.defineProperty( self, 'find',
	{

		value: function adapterFind ( data, callback )
		{

			var _l = {};

			_l.request = function ( done )
			{

				var req =
					{

						url: self.conf.baseUrl

						, method: 'GET'

					};

				if ( ! _.isUndefined( data ) )
				{

					if ( ! _.isUndefined( data.id ) )
					{

						req.url += '/' + data.id;

						delete data.id;

					}

					req.data = data;

					if ( _.isUndefined( data.paginate ) )
					{

						req.data.paginate=
						{

							page: data.paginate.page

							, limit: data.paginate.limit

						};

					}

				}

				self.request.do( req, function ( err, docs )
				{

					var out = [];

					if ( _.isEmpty( err ) )
					{

						_.each( docs, function ( d )
						{

							d = self.collection.Model( d );

							if ( _.isUndefined( self.cache.memory.storage[ d.id ] ) )
							{
								self.cache.memory.storage[ d.id ] = d;
							}

							out.push( d );

						} );

					}

					return done( err, out );

				} );

			};

			_l.sync = [ 'request', function ( done, res )
			{

				return self.cache.memory.sync( res.request, done );

			} ];

			var deferred;

			if ( _.isUndefined( callback ) )
			{
				deferred = m.deferred();
			}

			async.auto( _l, function ( err, results )
			{

				var owner = glhf[ self.collection.owner.type ][ self.collection.owner.identity ];

				owner.redraw();

				if ( glhf.env === 'development' )
				{
					console.log( 'glhfDataAdapter', err, results );
				}

				if ( _.isFunction( callback ) )
				{

					self.dataset = results.sync.synced;

					return callback.apply( self, [ err, results.sync.synced ] );

				}

				else
				{
					return deferred.resolve( results.request );
				}

			} );

			if ( _.isUndefined( callback ) )
			{
				return deferred.promise;
			}

		}

	} );

	self.resolveDiffer = function resolveDiffer ()
	{

	};

	return self;

}

module.exports = glhfDataAdapter;


'use strict';

var _ = require('lodash')
	, m = require('mithril')
	, async = require('async')
	, glhfXHRTransports = require('./glhfXHRTransports');

var glhfXHR = function glhfXHR ( conf )
{

	if ( _.isUndefined( conf.transport ) )
	{
		throw new Error('No transport is defined.');
	}

	var self = Object.create( this, { } );

	Object.defineProperty( self, 'conf', { value: conf } );

	Object.defineProperty( self, 'limit', { value: 5 } );

	Object.defineProperty( self, 'current', { writable: true, value: 0 } );

	// Default params checker.

	// public

	self.params = function requestParams ( done )
	{
		var req = this;

		if ( _.isUndefined( req.url ) )
		{
			return done( 'No req.url' );
		}

		if ( _.isUndefined( req.method ) )
		{
			req.method = 'GET';
		}

		return done();

	};

	if ( _.isFunction( conf.params ) )
	{
		self.params = conf.params;
	}

	self.request = glhfXHRTransports[ conf.transport ];

	self.do = function requestDo ( opts, callback )
	{

		if ( self.limit === self.current )
		{
			return _.noop();
		}

		self.current += 1;

		var task = {}
			, deferred;

		if ( _.isUndefined( callback ) )
		{
			deferred = m.deferred();
		}

		task.params = self.params.bind( opts );

		task.request = [ 'params', self.request.bind( opts ) ];

		async.auto( task, function ( err, res )
		{

			self.current -= 1;

			if ( _.isFunction( callback ) )
			{
				return callback( err, res.request );
			}

			else
			{
				return deferred.resolve( err || res.request );
			}

		} );

		if ( _.isUndefined( callback ) )
		{
			return deferred.promise;
		}

	};

	return self;

};

module.exports = glhfXHR;

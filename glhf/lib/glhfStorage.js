
'use strict';

var glhfStorage = {}
	, storages = {};

/**
 * The number of days until the cookie should expire
 */

var NB_EXPIRATION_DAYS = 10;

/**
 * Constants used to set specific storage type
 */
glhfStorage.COOKIE_STORAGE = "cookie";
glhfStorage.LOCAL_STORAGE = "local";
glhfStorage.SESSION_STORAGE = "session";
glhfStorage.IN_MEMORY_STORAGE = "in-memory";
glhfStorage.DEFAULT_STORAGE = "default";

/**
 * Constant as default storage index
 */
glhfStorage.DEFAULT_STORAGE_NAME = 'default';

/**
 * Default constructor of Cookie Store
 */
function SessionStorage( )
{
	this.$storage = window.sessionStorage;
}

/**
 * Stores the value for a given key
 * @param {string} key   a key
 * @param {object} value a value
 */
SessionStorage.prototype.set = function ( key , value )
{
	this.$storage.setItem( key , JSON.stringify( value ) );
};

/**
 * Returns the value given a key
 * @param {string} key a key
 */
SessionStorage.prototype.get = function ( key )
{
	return JSON.parse( this.$storage.getItem( key ) );
};

/**
 * Removes the key from the store
 * @param {string} key a key
 */
SessionStorage.prototype.remove = function ( key )
{
	this.$storage.removeItem( key );
};

/**
 * Returns true if the current browser has a sessionStorage
 */
SessionStorage.isAvailable = function( )
{
	return !!window.sessionStorage;
};

/**
 * Default constructor of in-memory Storage
 */
function InMemoryStorage( )
{
	this.$storage = {};
}

/**
* Stores the value for a given key
* @param {string} key   a key
* @param {object} value a value
*/
InMemoryStorage.prototype.set = function ( key , value )
{
	this.$storage[ key ] = value;
};

/**
* Returns the value given a key
* @param {string} key a key
*/
InMemoryStorage.prototype.get = function ( key )
{

	if( this.$storage[ key ] )
	{
		return this.$storage[ key ];
	}

	else
	{
		return undefined;
	}

};

/**
* Removes the key from the store
* @param {string} key a key
*/
InMemoryStorage.prototype.remove = function ( key )
{

	delete this.$storage[ key ];

};

/**
 * Default constructor of Local Storage
 */
function LocalStorage( )
{

	this.$storage = window.localStorage;

}

/**
* Stores the value for a given key
* @param {string} key   a key
* @param {object} value a value
*/
LocalStorage.prototype.set = function ( key , value )
{
	this.$storage.setItem( key , JSON.stringify( value ) );
};

/**
* Returns the value given a key
* @param {string} key a key
*/
LocalStorage.prototype.get = function ( key )
{
	return JSON.parse( this.$storage.getItem( key ) );
};

/**
* Removes the key from the store
* @param {string} key a key
*/
LocalStorage.prototype.remove = function ( key )
{
	this.$storage.removeItem( key );
};

/**
 * Returns true if the current browser has a localStorage
 */
LocalStorage.isAvailable = function( )
{
	return !!window.localStorage;
};

/**
 * Default constructor of Cookie Store
 */
function CookieStorage( )
{
	this.$storage = document.cookie = '';
}

/**
 * Stores the value for a given key
 * @param {string} key   a key
 * @param {object} value a value
 */
CookieStorage.prototype.set = function ( key , value )
{
	var expiration = new Date();
	var stringifiedValue = JSON.stringify( value );
	expiration.setTime( expiration.getTime() + ( NB_EXPIRATION_DAYS * 24*60*60*1000 ) );
	var expires = 'expires='+expiration.toUTCString();
	document.cookie = key + '=' + stringifiedValue + '; ' + expires;
};


/**
 * Returns the value given a key
 * @param {string} key a key
 */
CookieStorage.prototype.get = function ( key )
{
	var value = document.cookie.replace( new RegExp("/(?:(?:^|.*;\\s*)" + key + "\\s*\\=\\s*([^;]*).*$)|^.*$/"), "$1");
	if( value )
	{
		return JSON.parse( value.split('=')[1] );
	}
	else
	{
		return  "";
	}
};

/**
 * Removes the key from the store
 * @param {string} key a key
 */
CookieStorage.prototype.remove = function ( key )
{
	var name = key + "=";
	var splitCookies =  this.$storage.split( ';' );
	for( var i=0; i < splitCookies.length; i += 1 )
	{
		var cookie = splitCookies[ i ];
		while ( cookie.charAt( 0 ) === ' ' )
		{
			cookie = cookie.substring( 1 );
		}
		if (cookie.indexOf( name ) === 0 )
		{
			var cookieIndex = this.$storage.indexOf( cookie );
			this.$storage = this.$storage.slice( cookieIndex , cookieIndex + cookie.length );
		}
	}
	return "";
};

/**
 * Set the storage to the LocalStorage if exists
 * Otherwise set to the CookieStorage
 */
function $defaultStorage()
{

	var end;

	if( LocalStorage.isAvailable() )
	{
		end = storages[ glhfStorage.DEFAULT_STORAGE_NAME ] = new LocalStorage(  );
	}

	end = storages[ glhfStorage.DEFAULT_STORAGE_NAME ] = new CookieStorage(  );

	return end;

}

/**
* Set the current or namespaced  storage of given type
* @param {string} namespace The storage namespace
* @param
*/
glhfStorage.create = function( name , type )
{

	var storage;

	if( type )
	{

		switch( type )
		{

			case glhfStorage.COOKIE_STORAGE:
				storage = new CookieStorage();
				break;

			case glhfStorage.LOCAL_STORAGE:
				storage = new LocalStorage();
				break;

			case glhfStorage.SESSION_STORAGE:
				storage = new SessionStorage();
				break;

			case glhfStorage.IN_MEMORY_STORAGE:
				storage = new InMemoryStorage();
				break;

			case glhfStorage.DEFAULT_STORAGE:
				storage = $defaultStorage();
				break;

			default:
				throw new Error( type + ' is not a valid storage type.' );

		}

		storages[ name || glhfStorage.DEFAULT_STORAGE_NAME ] = storage;

	}

	return storages[ name || glhfStorage.DEFAULT_STORAGE_NAME ];

};

$defaultStorage();

module.exports = glhfStorage;

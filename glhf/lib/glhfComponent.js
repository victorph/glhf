
"use strict";

var _ = require('lodash')
	// , async = require('async')
	// , glhf = require('glhf')
	, m = require('mithril')
	// , moment = require('moment')
	, glhfCollection = require( 'glhfCollection' );

function glhfComponent ( model )
{

	function componentObject ()
	{

		var self = Object.create( null, {} );

		if ( _.isUndefined( model ) )
		{
			throw new Error( 'glhfComponent error no options...' );
		}

		if ( _.isUndefined( model.identity ) )
		{
			throw new Error( 'glhfComponent error no identity' );
		}

		Object.defineProperty( self, 'identity', { enumerable: false, configurable: false, writable: false, value: model.identity } );

		Object.defineProperty( self, 'view', { enumerable: false, configurable: false, writable: false, value: model.view } );

		Object.defineProperty( self, 'controller', { enumerable: false, configurable: false, writable: false, value: model.controller } );

		Object.defineProperty( self, 'redraw', { enumerable: false, configurable: false, writable: false, value: m.redraw } );

		Object.defineProperty( self, 'components', { enumerable: true, configurable: false, writable: true, value: model.components } );

		Object.defineProperty( self, 'collections', { enumerable: true, configurable: false, writable: true, value: model.collections } );

		Object.defineProperty( self, 'channels', { enumerable: true, configurable: false, writable: true, value: model.channels } );

		Object.defineProperty( self, 'dataset', { enumerable: true, configurable: false, writable: true, value: model.dataset } );

		Object.defineProperty( self, 'target', { enumerable: true, configurable: false, writable: true, value: model.target } );

		Object.defineProperty( self, 'controls', { enumerable: true, configurable: false, writable: true, value: model.controls } );

		Object.defineProperty( self, 'og', { enumerable: false, configurable: false, writable: false, value: model } );

		_.each( self.collections, function ( v, k )
		{

			v = _.get( window, v, undefined );

			if ( ! _.isUndefined( v ) )
			{
				self.collections[ k ] = new glhfCollection( v, { type: 'components', identity: model.identity } );
			}

		} );

		return self;

	}

	return new componentObject();

}

module.exports = glhfComponent;









'use strict';

/* global $:true */
/* global $:true */

var glhf = require('glhf');

var glhfXHRTransports =
{

	jquery: function glhfjQueryTransport ( done )
	{

		this.success = function ()
		{

			return done( null, arguments[ 0 ] );

		};

		this.error = function ()
		{

			if ( glhf.env === 'development' )
			{
				console.error( 'jQueryTransport error', arguments[ 0 ] );
			}

			return done( true );

		};

		return $.ajax( this );

	}

};

module.exports = glhfXHRTransports;
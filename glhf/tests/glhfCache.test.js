
var _ = require( 'lodash' )
	, async = require( 'async' )
	, assert = require( 'assert' )
	, Chance = require( 'chance' )
	, expect = require('chai').expect
	, should = require('chai').should()
	, chai = require('chai')
	, spies = require('chai-spies')
	, glhfCache = require( 'glhfCache' )
	, cache
	, owner = 'BarfoColleto';

chance = new Chance();

chai.use(spies);

var content=[];

for ( var i = 0; i < 92; i++ )
{

	content.push(
	{

		name: chance.name({ middle: true })

		, dob: chance.birthday( { year: chance.age() } )

		, web: chance.domain()

		, email: chance.email()

		, address: chance.address()

		, createdAt: chance.timestamp()

		, updatedAt: chance.timestamp()

		, id: chance.guid()

	} );

}

describe( 'glhfCache', function () {

	it( 'it should load glhfCache', function ()
	{

		expect(glhfCache).to.be.a('function');

	} );

	cache = glhfCache({ owner: owner });

	it( 'It should return its owner', function ()
	{

		assert.equal( cache.owner, owner );

	} );

	it( 'It should not let you change its owner', function ()
	{

		var newoner = 'OTHERCOLLETO';

		cache.owner = newoner;

		assert.notEqual( cache.owner, newoner );

	} );

	it( 'it should be able to set data', function ()
	{

		cache.set( 'some', 0 );

	} );

	it( 'it should be able to get the data', function ()
	{

		expect( cache.get('some') ).to.equal( 0 );

	} );

	it( 'it should be able to delete the data', function ()
	{

		expect( cache.remove('some') ).to.equal( undefined );

	} );

/*
	it( 'it should paginate the data', function ( done )
	{

		cache.set( 'dataset', content );

		cache.page( 'dataset', {

			data:
			{

				paginate:
				{

					page: 2

					, limit: 4

				}

			}

		}, function ( err, data )
		{

			should.not.exist( err );

			for( var i = 4; i < data.slice.length * 2; i++ )
			{

				var p1 = _.at( data.all, i )[ 0 ]
					, p2 = data.slice[ i - 4 ];

				expect( p1 ).to.deep.equal( p2 );

			}

			return done();

		} );

	} );
*/

	it('it should be able to compare based on their attributes data.', function ( done )
	{

		cache.compare( {

			all: content

			, with:
			[

				content[ chance.integer( { min: 1, max: 90 } ) ]

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt+=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt+=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt+=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt+=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt+=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt+=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt-=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt-=10000;

					return p;

				})()

				, (function(){

					var p = _.cloneDeep( content[ chance.integer( { min: 1, max: 90 } ) ] );

					p.updatedAt-=10000;

					return p;

				})()


			]

		}, function ( err, res )
		{

			done();

		} );

	} );

} );

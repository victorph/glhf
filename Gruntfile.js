/*

	load the app/pages folder and recursively load the index.js files. (default)

	let add multiple load paths for the pages/controllers.


*/

/*jslint node: true */

"use strict";

require('dotenv').load();

// var htmlparser = require("htmlparser2");

var
	// , async=require('async')
	path=require('path')
	, envify = require('envify/custom')
	// , fs=require('fs')
	// , util=require('util')
	, compile_tasks;

var glhf_browserify_envify =
{

	NODE_ENV: process.env.NODE_ENV

};

var glhf_browserify_files =
[

	'glhf/**/models/**/*.js'

	, 'models/**/*.js'

	, 'glhf/lib/glhf.js'

	, 'glhf/lib/glhfStorage.js'

	, 'glhf/lib/glhfSocket.js'

	, 'glhf/lib/glhfRouter.js'

	, 'glhf/lib/glhfCache.js'

	, 'glhf/lib/glhfPage.js'

	, 'glhf/lib/glhfComponent.js'

	, 'glhf/lib/glhfDataAdapter.js'

	, 'glhf/lib/glhfCollection.js'

	, 'glhf/lib/glhfXHRTransports.js'

	, 'glhf/lib/glhfXHR.js'

	, 'glhf/lib/glhfImageupload.js'

	// , 'app/client/**/front/**/*.js'

	, 'app/components/**/front/**/*.js'

	, 'app/pages/**/front/**/*.js'

];

var glhf_browserify_alias =
{

	'glhf': './glhf/lib/glhf.js'

	, 'glhfStorage': './glhf/lib/glhfStorage.js'

	, 'glhfSocket': './glhf/lib/glhfSocket.js'

	, 'glhfRouter': './glhf/lib/glhfRouter.js'

	, 'glhfCache': './glhf/lib/glhfCache.js'

	, 'glhfPage': './glhf/lib/glhfPage.js'

	, 'glhfComponent': './glhf/lib/glhfComponent.js'

	, 'glhfXHRTransports': './glhf/lib/glhfXHRTransports.js'

	, 'glhfXHR': './glhf/lib/glhfXHR.js'

	, 'glhfDataAdapter': './glhf/lib/glhfDataAdapter.js'

	, 'glhfCollection': './glhf/lib/glhfCollection.js'

	, 'glhfCollectionModel': './glhf/lib/glhfCollectionModel.js'

	, 'glhfImageupload': './glhf/lib/glhfImageupload.js'

};

var glhf_browserify_paths =
[

	'./glhf/'

	, './glhf/lib/'

	, './app/'

	// , './app/components/'

	, './node_modules/'

];

switch ( process.env.NODE_ENV )
{

	case 'development':

		compile_tasks =
		[

			'shell:notify_start'

			, 'clean:build'

			, 'browserify:app'

			, 'concat:deps'

			, 'jade:views'

			, 'jade:components'

			, 'jade:pages'

			, 'concat:build'

			, 'less:build'

			, 'concat:css'

			, 'shell:notify_end'

		];

		break;

	case 'production':

		compile_tasks =
		[

			'clean:build'

			, 'browserify:app'

			// , 'jshint:build'

			, 'concat:deps'

			, 'jade:views'

			, 'jade:components'

			, 'jade:pages'

			, 'concat:build'

			, 'less:build'

			, 'concat:css'

			, 'uglify:build'

		];

		break;

	case 'test':
		break;

	default:

}

/*
function parseJade ( src )
{
	var parser = new htmlparser.Parser({
		onopentag: function(name, attribs){
			console.log(name,attribs);
		},
		onattribute: function(name, value){
			console.log("attribute:", name, value);
		},
		ontext: function(text){
			console.log("-->", text);
		},
		onclosetag: function(tagname){
			console.log(tagname);
		}
	}, {decodeEntities: true});
	parser.write(src);
	return parser.end();
}
*/

module.exports = function(grunt)
{

	grunt.loadNpmTasks('grunt-contrib-jshint');

	grunt.loadNpmTasks('grunt-karma');

	grunt.loadNpmTasks('grunt-contrib-clean');

	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.loadNpmTasks('grunt-bower-task');

	grunt.loadNpmTasks('grunt-contrib-jade');

	grunt.loadNpmTasks('grunt-contrib-less');

	grunt.loadNpmTasks('grunt-browserify');

	grunt.loadNpmTasks('grunt-shell');

	// grunt.registerTask( 'compile', compile_tasks );

	grunt.registerTask( 'default', compile_tasks );

	// grunt.loadNpmTasks('grunt-karma');

	// Project configuration.
	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json')

		, shell:
		{

			options:
			{
				stderr: false
			}

			, notify_start:
			{
				command: "osascript -e 'display notification \"Grunt compiling has started.\" with title \"GLHF Grunt\"'"
			}

			, notify_end:
			{
				command: "osascript -e 'display notification \"Grunt compile completed.\" with title \"GLHF Grunt\"'"
			}

		}

		, browserify:
		{

			options:
			{

				transform:
				[

					"browserify-shim"

					, envify( glhf_browserify_envify )

				]

				, browserifyOptions:
				{

					paths: glhf_browserify_paths

				}

				, alias: glhf_browserify_alias

			}

			, app:
			{

				files:
				{

					'public/client/source/glhf.js': glhf_browserify_files

				}

			}

		}

		, bower:
		{

			install:
			{

				options:
				{

					install: true

					, copy: false

					, layout: 'byType'

					, targetDir: 'bower_components'

					, cleanTargetDir: true

				}

			}

		}

		, jade:
		{

			views:
			{

				options:
				{

					client: true

					, namespace: 'JST.app'

					, data:
					{
						debug: process.env.NODE_ENV === 'development' ? true : false
					}

					, processName: function ( fn )
					{

						// console.log( fn );

						fn = path.basename( fn, path.extname( fn ) );

						return fn.replace( /\W+/g, '' );

					}

				},

				files:
				{

					"public/client/views/general.js":
					[

						"app/views/**/*.jade"

					]

				}

			}

			, pages:
			{

				options:
				{

					client: true

					, namespace: 'JST.page'

					, data:
					{
						debug: process.env.NODE_ENV === 'development' ? true : false
					}

					, processName: function ( fn )
					{

						// console.log( fn );

						fn = path.basename( fn, path.extname( fn ) );

						return fn.replace( /\W+/g, '' );

					}

				},

				files:
				{

					"public/client/views/pages.js":
					[

						"app/pages/**/*.jade"

					]

				}

			}

			, components:
			{

				cwd: './app/components/'

				, options:
				{

					client: true

					, namespace: 'JST.component'

					, data:
					{
						debug: process.env.NODE_ENV === 'development' ? true : false
					}

					, processName: function ( fn )
					{

						console.log( fn );

						var parse = path.parse( fn );

						console.log( parse );

						fn = path.basename( fn, path.extname( fn ) );

						return fn.replace( /\W+/g, '' );

					}

				},

				files:
				{

					"public/client/views/components.js":
					[

						"app/components/**/*.jade"

					]

				}

			}

		}

		, less:
		{

			build:
			{

				options:
				{

					plugins:
					[

						// new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
						new ( require( 'less-plugin-clean-css' ) )( { advanced: true } )

					]

					// , sourceMap: true

					// , sourceMapFilename: "public/css/style.map.css"

				},

				files:
				{

					"public/css/style.css":
					[

						"bower_components/fuelux/less/fuelux.less"

						, "app/styles/main.less"

						, "app/pages/**/styles/**/*.less"

						, "app/components/**/styles/**/*.less"

					]

				}

			},

		}

		, jshint:
		{

			options:
			{
				jshintrc: true
			}

			, all:
			[
				'Gruntfile.js'
			]

			, build:
			{

				src:
				[

					'public/**/*.js'

					, 'app/components/**/front/**/*.js'

					, 'app/pages/**/front/**/*.js'

				]

			}

		}

		, concat:
		{

			options:
			{

				separator: ';'

				, stripBanners:
				{

					block: true

					, line: true

				}

			}

			, home:
			{

				src:
				[

					// '/public/lib/typeahead/dist/typeahead.bundle.min.js'

				]

				, dest: 'public/client/build/home.js'

			}

			, account_profile:
			{

				src:
				[

					'public/libs/geocomplete.js'

					// , 'public/lib/typeahead/dist/typeahead.bundle.min.js'

					// , 'public/lib/tagmanager/tagmanager.js'

				]

				, dest: 'public/client/build/signup.js'

			}

			, deps:
			{

				src:
				[

					'bower_components/lodash/lodash.js'

					// , 'bower_components/async/lib/async.js'

					//

					, 'bower_components/jade/runtime.js'

					// , 'bower_components/mithril/mithril.js'

					// , 'public/libs/mithril-storage.min.js'

					// , 'bower_components/mio/dist/mio.js'

					// , 'bower_components/socket.io/lib/client.js'

					// , 'bower_components/mio-ajax/dist/mio-ajax.js'

					// , 'bower_components/eventemitter2/lib/eventemitter2.js'

					// , 'bower_components/chance/chance.js'

					, 'bower_components/dropzone/dist/dropzone.js'

					, 'public/libs/bCrypt.js'

					, 'public/libs/isaac.js'

					, 'bower_components/jquery/dist/jquery.js'

					, 'bower_components/bootstrap/dist/js/bootstrap.js'

					, 'bower_components/fuelux/dist/js/fuelux.min.js'

					, "bower_components/mingo/mingo.js"

				],

				dest: 'public/client/build/libs.js'

			}

			, build:
			{

				src:
				[

					'public/client/build/libs.js'

					, 'public/client/source/glhf.js'

					, 'public/client/views/general.js'

					, 'public/client/views/components.js'

					, 'public/client/views/pages.js'

				]

				, dest: 'public/client/build/app.js'

			}

			, css:
			{

				options:
				{

					separator: '\n'

				}

				, src:
				[

					// "bower_components/bootstrap/dist/css/bootstrap.min.css"

					'bower_components/dropzone/dropzone.css'

					, 'public/css/style.css'

				]

				, dest: "public/css/styles.css"

			}

		}

		, uglify:
		{

			options:
			{

				banner:
				[

					'/*!\n'

					, '<%= pkg.name %> v<%= pkg.version %>\n'

					, 'Authors:\n<%= pkg.authors %>\n'

					, '\n'

					, 'Last official build date: <%= grunt.template.today("dd-mm-yyyy") %>\n'

					, '*/\n'

				].join('')

				, compress: true

				, sourceMap: true

				, sourceMapName: 'public/client/build/app.map.js'

			}

			, build:
			{

				files:
				{

					'public/client/build/app.js':
					[

						'public/client/build/libs.js'

						, 'public/client/build/glhf.js'

					]

				}

				, options:
				{

					mangle: true

				}

			}

		}

		, clean:
		{

			build:
			{

				src:
				[

					'public/client/build/**/*.js'

					, 'public/client/source/**/*.js'

					, 'public/client/views/**/*.js'

					, 'public/**/*.css'

					, 'public/**/*.map'

					, '!.gitkeep'

				]

			}

		}

		, watch:
		{

			app:
			{

				files:
				[

					'app/**/*'

					, 'glhf/**/*'

					, 'Gruntfile.js'

					, '!**/routes/**'

					, '!**/middleware/**'

					, '!**/roles/**'

					, '!**/channels/**'

					, '!**/node/**'

				]

				, tasks: compile_tasks

				, options:
				{

					atBegin: true

					, livereload: false

					, spawn: false

					, debounceDelay: 180

				}

			}

		}

		, karma:
		{

			options:
			{

				configFile: 'karma.conf.js'

				, port: 9999

				, singleRun: false

			}

			, app:
			{


			}

		}

	});

};


"use strict";

require('dotenv').load();

var _=require('lodash')
	// , async=require('async')
	, glob = require("glob")
	, io = require('socket.io')
	, cookieParser = require('cookie-parser')
	, sockets = {};

// small hack to be able to parse the request cookies.

function IOcookieParser ()
{
	var parser = cookieParser.apply( null, arguments );
	return function ( socket, next ) { parser( socket.request, null, next ); };
}

module.exports = function WebSocketServer ( server, app )
{

	io = io( server );

	io.use( new IOcookieParser( process.env.SES_SECRET ) );

	io.use( function ( socket, next )
	{

		// console.log( app.models ); // Works!

		// console.log( 'authorization cookies', socket.request.cookies );

		// console.log( 'authorization signedCookies', socket.request.signedCookies );

		app.models.sessions.find( socket.request.signedCookies[ 'glhf.sid' ], function ( err, ses )
		{

			if ( ! err && ses.length > 0 )
			{

				socket.request.session = ses[ 0 ].session;

				// delete socket.request.session.cookie;

			}

			return next();

		} );

	} );

	io.use( function ( socket, next )
	{

		console.log( socket.request.session );

		if ( socket.request.session )
		{

			socket.request.session.some = 'barfa';

		}

		return next();

	} );

	io.on( 'connection', function ( socket )
	{

		console.log('socket connected:',socket.request);
		console.log('socket connected:',socket.id);

		socket.on('disconnect', function( )
		{

			console.log('socket disconnected:',socket.request);
			console.log('socket disconnected:',socket.id);

		} );

	} );

	// Mounting the found sockets.

	glob( "./app/**/sockets/**/*.js", function ( er, files )
	{

		_.each( files, function ( skt )
		{

			var socket = require( skt );

			console.log( 'Mounting socket %s', '/' + socket.name );

			socket.do( io.of( '/' + socket.name ) );

		} );

	} );

};



// EOF

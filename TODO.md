
# TODO

###### (Im leaving the DONE! comments, to document the features later...) ######

***

## Cluster: ##
* http://stackoverflow.com/questions/2387724/node-js-on-multi-core-machines
* http://learnboost.github.io/cluster/docs/api.html

***

Add validate to the collection.

***

Needs to have different views per zone, if not the page is going to be loading a lot of unneeded stuff everywhere.
but you cant load JavaScript on the fly so... It needs to reload the page at this zones.

***

##### (DONE!) #####

Make glhfDataAdapter use either Socket, Channel, Or XHR.

***

##### (DONE!) #####

Add an active path to glhf and add the active parts (pages/components/channels/etc) there
so every page has its active state and we can snapshot it and destroy it easily.

Added an off thing to the components. that whenever the page changes they are called. for now.

***

##### (DONE!) #####

Re structure the app:

	app/component/NAME/front // instead of lib
	app/page/NAME/front // instead of lib

	app/page/router // index.js holds all the routes for Express
	app/routes // General Express routing, It adds the routes above any of /app/page/
	app/middlewares // common middle-ware
	app/roles // common roles

***

## CSS/LESS ##

##### Not so urgent. #####

* Remove Uglify/Minify from the CSS file on development environment...
* Add middleware directly to it so its broken down in files.
* Check venify

***

Remove the conf allowed for routes from the models. add roles.

***

### Roles ###

	Add user needed roles guest, staff, owner, superuser, student, root, etc as as middle-ware code more than a DB thing. /app/roles/root.js

***

##### (This is how it is done right now.) #####

Use mithril to generate the pages and components most basic bones.

***

make the search work like this:
	search in the memory
	search in the cache
	search in json generated related to my profile related sub docs.
	search in json of the main json dataset
	:)

***

***

+ ## glhf

***

+ ## glhfCache

	* Base code is almost finished, Just needs the programmatic tests to be sure and be final.

***

+ ## glhfDatAdapter

	* Write tests

	* glhfChannel integration.

***

+ ## glhfRequest

	* it should be able to switch transports depending on the glhfCollection configuration.

	* it should be able to switch transports depending on the glhfCollectionModel configuration.

***

+ ## glhfRequestTransports

	* Add the glhfWSocket transport

***

+ ## glhfCollection

	* Add transport configuration

***

+ ## glhfCollectionModel

	* Add transport configuration

***

+ ## glhfSocket - WebSocket Interface to listen and emit.

	* It should post trough glhfDatAdapter

	* It should put trough glhfDatAdapter

	* It should delete trough glhfDatAdapter

	* It should get trough glhfDatAdapter

	It should be inside glhfChannel alone and in glhfRequest as a transport.
	In the transport part it only needs the emitter.
	In the channel needs both listener and emitter.

***

+ ## glhfStorage

	* Write tests

	* Clean up the code.

***

# NOTE: The smartest thing to do is to leave out this for now, And use Mithril for the controllers and views.

	Because after this point the below features are cool but not needed for production.
	Because this are classes of higher level.

***

+ ## glhfPage

	* Add channels

	* Add page navigation

	* Add collection relation.

	* DOM Refresh events

	A page should be able to hold many components.

***

+ ## glhfComponent

	* Add channel(s)

	* Add component basic controls. (Create, Save, Sync, Meaning the Collection.).

	* DOM Refresh events

	* Add collection(s) relation. (Should be able to accept an array.).

		* Needs shared option so it wont update glhfPage view if its shared with Page.

	A component should be able to be loaded, refreshed and destroyed on demand.

***

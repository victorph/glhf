
"use strict();";

require('dotenv').load();

var _ = require('lodash')
	, fs = require('fs')
	, util = require('util')
	, Waterline = require('waterline')
	, mongoAdapter = require('sails-mongo')
	, orm = new Waterline()
	, config =
	{

		adapters:
		{

			'mongo': mongoAdapter

		}

		, connections: {}

		, defaults:
		{

			migrate: 'alter'

			, autoCreatedAt: true

			, autoUpdatedAt: true

		}

	};

if ( process.env.NODE_ENV == 'production' )
{

	config.connections.default =
	{

		adapter: process.env.DB_PROD_ADAPTER || 'mongo'

		, host: process.env.DB_PROD_HOST || 'localhost'

		, database: process.env.DB_PROD_NAME || 'glhf_prod'

	};

}

if ( process.env.NODE_ENV == 'development' )
{

	config.connections.default =
	{

		adapter: process.env.DB_DEV_ADAPTER || 'mongo'

		, host: process.env.DB_DEV_HOST || 'localhost'

		, database: process.env.DB_DEV_NAME || 'glhf_dev'

	};

}

if ( process.env.NODE_ENV == 'test' )
{

	config.connections.default =
	{

		adapter: 'mongo'

		, host: 'localhost'

		, database: 'glhf_test'

	};

}

module.exports = function ( opts, done )
{

	var path = util.format( "%s/%s", process.cwd(), process.env.DB_PATH_MODELS )
		, schemas = {};

	fs.readdir( path, function ( err, files )
	{

		if ( err ) throw err;

		_.each( files, function ( item )
		{

			var schema = require( util.format( "%s/%s", path, item ) );

			schemas[ schema.identity ] = schema;

			// if (process.env.NODE_ENV=='development') console.log('loading %s',schema.identity);

			orm.loadCollection( Waterline.Collection.extend( schema ) );

		} );

		orm.initialize( opts || config, function ( err, models )
		{

			if ( err ) throw err;

			return done( err, models.collections, schemas );

		});

	} );

};

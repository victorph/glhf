
"use strict";

require('dotenv').load();

var _ = require('lodash')
	, async = require('async');

if (process.env.NODE_ENV==='development')
{
	console.log("API has loaded");
}

// All purpose array check for docs, And response, It calls the done that is attached to this via bind.

var returnDocs = function returnDocs ( err, docs )
{

	if ( ! _.isArray( docs ) )
	{
		docs = [ docs ];
	}

	return this.done( err, _.isEmpty( err ) ? docs : [] );

};

// All purpose async.auto done callback.

var autoDone =  function autoDone ( err, results )
{

	if ( _.isEmpty( err ) )
	{

		return this.res.json( results.do );

	}

	else
	{

		if ( err.error === "E_VALIDATION" )
		{
			return this.res.status( 400 );
		}

		else
		{
			return this.res.status( 500 );
		}

		// TODO: change this for a more slim response.
		if ( process.env.NODE_ENV === 'production' )
		{
			return this.res.send( err.error );
		}

		// dev mode answer.
		else
		{
			return this.res.send( err );
		}

	}

};

var self =
{

	count: function APICount ( app, req, res )
	{

		var _l={}
			, model;

		_l.check = function ( done )
		{

			if ( _.isUndefined( req.body ) )
			{

				return done( 'This is not a post?' );

			}

			model = app.models[ req.params.model ];

			if ( _.isUndefined( model ) )
			{

				return done( 'That model doest not exists.' );

			}

			return done( null, true );

		};

		_l.count = [ 'check', function ( done )
		{

			return model.count({}).exec(done);

		} ];

		return async.auto( _l, function ( err, re )
		{

			return res.json( re.count );

		} );

	}

	, get: function APIGet ( app, req, res, cb )
	{

		var _l={}
			, model
			, schema
			, allowed=[];

		_l.check = function ( done )
		{

			if ( _.isUndefined( req.body ) )
			{
				return done( 'This is not a post?' );
			}

			model = app.models[ req.params.model ];

			if ( _.isUndefined( model ) )
			{
				return done( 'That model doest not exists.' );
			}

			schema = app.schemas[ model.identity ];

			if ( ! _.isUndefined( schema.conf ) && ! _.isUndefined( schema.conf.allowed ) && ! _.isUndefined( schema.conf.allowed.get ) )
			{
				allowed = schema.conf.allowed.get;
			}

			return done( null, true );

		};

		_l.do = [ 'check', function ( done )
		{

			var find = {}
				, q;

			if ( _.isString( req.query.id ) )
			{
				find.id = req.query.id;
			}

			q = model.find( find );

			// if ( ! _.isEmpty( req.body.populate ) ) _.each( req.body.populate, function ( p ) { q.populate( p ); } );

			if ( ! _.isEmpty( req.query.limit ) )
			{
				q.limit( req.query.limit );
			}

			if ( ! _.isEmpty( req.query.paginate ) )
			{

				if ( ! _.isUndefined( req.query.paginate.page ) && ! _.isUndefined( req.query.paginate.limit ) )
				{
					q.paginate( req.query.paginate );
				}

			}

			q.exec( returnDocs.bind( { done: done } ) );

		} ];

		async.auto( _l, autoDone.bind( { res: res, cb: cb } ) );

	}

	, getOne: function APIGetOne ( app, req, res, cb )
	{

		var _l={}
			, model;

		_l.check = function ( done )
		{

			if ( _.isUndefined( req.body ) )
			{
				return done( 'This is not a post?' );
			}

			model = app.models[ req.params.model ];

			if ( _.isUndefined( model ) )
			{
				return done( 'That model doest not exists.' );
			}

			if ( _.isUndefined( req.params.modelid ) )
			{
				return done( 'param modelid is not set.' );
			}

			return done( null, true );

		};

		_l.do = [ 'check', function ( done )
		{

			var find =
				{
					id: req.params.modelid
				}
				, q;

			q = model.find( find );

			q.limit(1);

			q.exec( returnDocs.bind( { done: done } ) );

		} ];

		async.auto( _l, autoDone.bind( { res: res, cb: cb } ) );

	},

	post: function APIPost ( app, req, res, cb )
	{

		var _l = {}
			, model
			, schema
			, allowed=[];

		_l.check = function ( done )
		{

			if ( _.isUndefined( req.body ) )
			{
				return done( 'This is not a post?' );
			}

			model = app.models[ req.params.model ];

			if ( _.isUndefined( model ) )
			{
				return done( 'That model doest not exists.' );
			}

			schema = app.schemas[ model.identity ];

			if ( ! _.isUndefined( schema.conf ) && ! _.isUndefined( schema.conf.allowed ) && ! _.isUndefined( schema.conf.allowed.post ) )
			{
				allowed = schema.conf.allowed.post;
			}

			return done( null );

		};

		_l.do = [ 'check', function ( done )
		{

			_.each( req.body, function ( val, key )
			{

				// checks if this path is allowed via post
				if ( ! _.includes( allowed, key ) )
				{
					delete req.body[ key ];
				}

			} );

			model.create( req.body , function( err, doc )
			{

				if ( _.isEmpty( err ) )
				{
					return done( null, [ doc ] );
				}

				return done( err );

			});

		} ];

		async.auto( _l, autoDone.bind( { res: res, cb: cb } ) );

	},

	put: function APIPut ( app, req, res, cb )
	{

		var _l = {}
			, model
			, schema
			, allowed=[];

		_l.check = function ( done )
		{

			if ( _.isUndefined( req.body ) )
			{
				return done( 'This is not a post?' );
			}

			model = app.models[ req.params.model ];

			if ( _.isUndefined( model ) )
			{
				return done( 'That model doest not exists.' );
			}

			schema = app.schemas[ model.identity ];

			if ( ! _.isUndefined( schema.conf ) && ! _.isUndefined( schema.conf.allowed ) && ! _.isUndefined( schema.conf.allowed.put ) )
			{
				allowed = schema.conf.allowed.put;
			}

			return done( null );

		};

		_l.do = [ 'check', function ( done )
		{

			var find =
				{
					id: req.params.modelid
				}
				, q;

			q = model.find( find );

			q.limit(1);

			q.exec( function ( err, res )
			{

				if ( _.size( res ) > 0 )
				{

					_.each( req.body, function ( val, key )
					{

						// checks if this path is allowed via put
						if ( _.includes( allowed, key ) )
						{
							res[ 0 ][ key ] = val;
						}

					} );

					res[ 0 ].save( function( err, doc )
					{

						return done( err, [ doc ] );

					});

				}

			} );

		} ];

		async.auto( _l, autoDone.bind( { res: res, cb: cb } ) );

	},

	delete: function APIDelete ( app, req, res, cb )
	{

		var _l = {}
			, model;

		_l.check = function ( done )
		{

			if ( _.isUndefined( req.body ) )
			{
				return done( 'This is not a post?' );
			}

			model = app.models[ req.params.model ];

			if ( _.isUndefined( model ) )
			{
				return done( 'That model doest not exists.' );
			}

			return done( null );

		};

		_l.do = [ 'check', function ( done )
		{

			var what = { id: req.params.modelid };

			model.destroy( what, function( err, doc )
			{

				if ( _.isEmpty( err ) )
				{
					return done( null, doc );
				}

				return done( err );

			});

		} ];

		async.auto( _l, autoDone.bind( { res: res, cb: cb } ) );

	}

};

module.exports = self;

"use strict";

require('dotenv').load();

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val)
{

	var port = parseInt( val, 10 );

	if (isNaN(port))
	{
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;

}

var _ = require('lodash')
	,express = require('express')
	, path = require('path')
	, favicon = require('serve-favicon')
	, _logic = {}
	, morgan = require('morgan')
	, async = require('async')
	, glob = require('glob')
	// , request = require('request')
	, http = require('http')
	, cookieParser = require('cookie-parser')
	, bodyParser = require('body-parser')
	, methodOverride = require('method-override')
	, session = require('express-session')
	, MongoStore = require('connect-mongo')(session)
	, compression = require('compression')
	, uuid = require('node-uuid')
	, passport = require('passport')
	, LocalStrategy = require('passport-local').Strategy
	, port = normalizePort( process.env.PORT || '3000' )
	, api = require('./api')
	, app = express()
	, server;

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error)
{

	if (error.syscall !== 'listen')
	{
		throw error;
	}

	var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

	// handle specific listen errors with friendly messages
	switch (error.code)
	{

		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;

		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;

		default:
			throw error;

	}

}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening()
{

	var addr = server.address();

	var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

	if ( process.env.NODE_ENV==='development')
	{

		// debug('Listening on ' + bind);

		console.log( 'Listening on ' + bind );

	}

}

app.use( function ( req, res, next )
{

	req.id = uuid.v4();

	res.locals.env = process.env.NODE_ENV;

	// res.setHeader( 'X-Server', 'GLHF 1.0.0' );

	// res.setHeader( 'X-Powered-By', 'GLHF' );

	// res.setHeader('Content-Type', 'Application/JSON');

	res.setHeader('Access-Control-Allow-Origin', process.env.XREQAllowFrom );

	res.setHeader('Access-Control-Allow-Headers','X-Requested-With');

	res.setHeader('Access-Control-Expose-Headers','Location');

	res.setHeader('Access-Control-Expose-Headers','Expires');

	next();

} );



app.set( 'env', process.env.NODE_ENV );

app.set( 'views', path.join( __dirname, 'app/views' ) );

app.set( 'view engine', 'jade' );

app.set( 'port', process.env.PORT );

app.use( favicon( __dirname + '/public/favicon.ico' ) );

morgan.token( 'id', function getId ( req ) { return req.id; } );

if ( app.get('env') === 'development' )
{
	morgan.format( 'custom', ':response-time :url :method :status' );
}

else
{
	morgan.format( 'custom', ':date[web] - :id - :status - :method - :url - :response-time ms' );
}

app.use( morgan( 'custom' ) );

app.logger = morgan;

app.use( methodOverride() );

app.use( bodyParser.json() );

app.use( bodyParser.urlencoded( { extended: false } ) );

app.use( cookieParser( process.env.SES_SECRET ) );

app.use(session({
	cookie: { maxAge: 1000*60*2 }
	, secret: process.env.SES_SECRET
	, resave: true
	, saveUninitialized: true
	, name: 'glhf.sid'
	, genid: function()
	{
		return uuid.v4();
	}
	, store: new MongoStore(
	{
		db: process.env.NODE_ENV === 'development' ? process.env.DB_DEV_NAME : process.env.DB_PROD_NAME
		, host: process.env.NODE_ENV === 'development' ? process.env.DB_DEV_HOST : process.env.DB_PROD_HOST
		// , port: 10065
		// , username: 'cm'
		// , password: 'cm'
		, collection: 'sessions'
		, autoRemove: 'native'
		, autoReconnect: true
	} )
}));

if ( process.env === 'production' )
{
	app.use( compression( { level: 8 } ) );
}

app.use( express.static( path.join( __dirname, 'public' ) ) );

app.use( passport.initialize() );

app.use( passport.session() );

app.post( '/gate/local', passport.authenticate('local'), function(req, res) {

	res.redirect('/dashboard');

});

_logic.db = function ( done )
{

	require( './config/db' )( null, function ( err, models, schemas )
	{

		app.models = models;

		app.schemas = schemas;

		return done( null, true );

	} );

};

_logic.api = [ 'db', function ( done )
{

	app.get( '/api/:model/count', async.apply( api.count, app ) );

	app.get( '/api/:model', async.apply( api.get, app ) );

	app.get( '/api/:model/:modelid', async.apply( api.getOne, app ) );

	app.patch( '/api/:model/:modelid', async.apply( api.put, app ) );

	app.post( '/api/:model', async.apply( api.post, app ) );

	app.put( '/api/:model/:modelid', async.apply( api.put, app ) );

	app.delete( '/api/:model/:modelid', async.apply( api.delete, app ) );

	return done( null, true );

} ];

_logic.passport = [ 'db', function( done )
{

	passport.serializeUser( function ( user, done )
	{

		done( null, user.id );

	});

	passport.deserializeUser( function( id, done )
	{

		// app.models.users.findById( id, function( err, user )
		app.models.users.findById( id, function( err )
		{

			done( err, id );

		} );

	} );

	return done();

} ];

_logic.local = [ 'db', function ( done )
{

	app.post( '/gate/local', passport.authenticate( 'local', { successRedirect: '/dashboard', failureRedirect: '/auth/login' }) );

	passport.use( new LocalStrategy( function ( username, password, next )
	{

		app.models.users.findOne( { email: username }, function ( err, user )
		{

			if ( err )
			{
				return next( err );
			}

			if ( _.isEmpty( user ) )
			{
				return next( null, false );
			}

			// check pass

			return next( null, user );

		} );

	}));

	return done( null, true );

} ];

_logic.facebook = [ 'db', function ( done )
{

	return done( null, true );

} ];

_logic.google = [ 'db', function ( done )
{

	return done( null, true );

} ];

function __loadRoutes ( callback )
{

	var mounted=[]
		, _logic={};

	_logic.app = function ( done )
	{
		return glob( "./app/routes/index.js", function ( er, files )
		{
			_.each(files, function ( fn ) {
				app.use( '/', require(fn)( app ) );
				mounted.push(fn);
			});
			return done( er, files );
		} );
	};

	_logic.pages = [ 'app', function ( done )
	{
		return glob( "./app/pages/**/router/index.js", function ( er, files )
		{
			_.each(files, function ( fn ) {
				var index = require(fn)( app );
				app.use( '/', index );
				mounted.push(fn);
			});
			return done( er, files );
		} );
	} ];

	async.auto( _logic, function ( err ) { return callback( err, mounted ); });

	return;

}

_logic.pages = [ 'local', __loadRoutes  ];

_logic.errors = [ 'pages', function ( done )
{

	app.use( function ( req, res, next )
	{

		// console.log(req.originalUrl); // '/admin/new'
		// console.log(req.baseUrl); // '/admin'
		// console.log(req.path); // '/new'

		var err = new Error( http.STATUS_CODES[ 404 ] );

		err.status = 404;

		err.url = req.originalUrl;

		next( err );

	});

	// error handler

	// app.use( function ( err, req, res, next )
	app.use( function ( err, req, res )
	{

		res.status( err.status || 500 );

		res.render( 'error', { message: err } );

	} );

	return done( null, true );

} ];

_logic.servers = [ 'errors', function ( done )
{

	app.set( 'port', port );

	server = http.createServer( app );

	server.listen(port);

	require( './appWS.js' )( server, app );

	server.on('error', onError);

	server.on('listening', onListening);

	return done( null, true );

} ];

async.auto( _logic, function ( err, results )
{

	if (process.env.NODE_ENV === 'development')
	{
		console.log( 'Backend init', results );
	}

} );

process.once('SIGUSR2', function ()
{

	console.log('here');

	console.log(arguments);

	process.kill( process.pid, 'SIGUSR2' );

});

